var rooms = require('rooms');
var myFunctions = require('functions');

var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {

        
        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);

        if(creep.memory.building && creep.carry.energy == 0) {
            // if the creep is building and runs out of energy, go get energy
            creep.memory.building = false;
            creep.say('Work work.');
        }
        if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
            // if the creep is getting energy and has topped off, go build
            creep.memory.building = true;
            creep.say('Hammer time.');
        }

        //console.log(rooms[0] + rooms[1]);
        //console.log(Game.rooms.rooms[0].room.name);
        //console.log(rooms.home);
        /*for (var i in Game.rooms) {
            console.log(Game.rooms[i].name);
        }*/

        if(creep.memory.building) {

            // attempt to find open construction sites
            var currentRoomTargets = creep.room.find(FIND_CONSTRUCTION_SITES);
            try {
                if (Game.flags.MyRoom1) {
                    var otherRoomTargets = Game.flags.MyRoom1.room.find(FIND_CONSTRUCTION_SITES);
                }
            } catch (err) {
                //console.log(err.stack);
            }

            //console.log("Current room length: " + currentRoomTargets.length);
            //console.log("Other room length: " + otherRoomTargets.length);
            
            if(currentRoomTargets.length) {

                //var target = creep.pos.findClosestByPath(currentRoomTargets);
                var target = creep.pos.findClosestByRange(currentRoomTargets);
                if(creep.build(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }

            } else if (otherRoomTargets && otherRoomTargets.length) {
                
                if (creep.room.name != Game.flags.MyRoom1.room.name) {
                    creep.moveTo(Game.flags.MyRoom1);
                }
                /*console.log(otherRoomTargets);
                var target = creep.pos.findClosestByPath(otherRoomTargets);
                //console.log("Target Room: " + target.room.name);
                console.log("target: " + target);
                var result = creep.build(target);
                //console.log("Result: " + result);
                //console.log("Target X: " + target.pos.x + " Y: " + target.pos.y);
                if(result == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }*/
            } else {
                // if no construction sites exist, go upgrade the controller
                if (creep.room.name != creep.memory.room) {
                    try {
                        creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
                    } catch (err) {
                        Game.notify('Error in new builder spawn go code: ' + err.message);
                    }
                } else if (creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller);
                }

            }
        } else {

            // when out of energy, look for a storage to get energy from
            var storages = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
              }
            });
            if (storages.length > 0) {

                var target = creep.pos.findClosestByPath(storages);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }

            } else {

                // if the storage doesn't have enough energy, look for a container that does
                var containers = creep.room.find(FIND_STRUCTURES, {
                  filter: (structure) => {
                      return (structure.structureType == STRUCTURE_TOWER ||
                              structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) > creep.carryCapacity;
                  }
                });
                if(containers.length > 0) {
                    var target = creep.pos.findClosestByPath(containers);
                    if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target);
                    }
                } else {
                    // if no storages of containers have enough energy, go sit around until they do
                    //creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
                    /* NO LUCK ANYWHERE PULL FROM THE SPAWN*/
                    var spawn = creep.room.find(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return (structure.structureType == STRUCTURE_SPAWN) && structure.energy > creep.carryCapacity;
                        }
                    });
                    if (spawn.length > 0) {
                        if (creep.withdraw(spawn[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                            creep.moveTo(spawn[0]);
                        }
                    } else {
                        creep.moveTo(Game.spawns['Zone1']);
                    }
                    
                }

            }

        }
    }
};

module.exports = roleBuilder;