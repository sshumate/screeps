var rooms = require('rooms');
var myFunctions = require('functions');

var roleExpeditionHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {

        function setMineInfo() {
            var newMineSource = 0;
            var mineRoom;
            var roomName = creep.memory.room;
            
            var exp_harvesters = _.filter(Game.creeps, (guy) => { return guy.memory.role == 'exp_harvester' && guy.memory.room == roomName && guy.memory.fading == false });
            
            if (roomName == 'W12S63') {
                // main room
                // 2 harvesters, one to W12S64 source 0, one to W11S64 source 0
                
                var W11count = 0,
                    W12count = 0;
                
                for (var creepid in exp_harvesters) {
                    if ( exp_harvesters[creepid].memory.destination == 'W12S64' ) {
                        W12count += 1;
                    } else if ( exp_harvesters[creepid].memory.destination == 'W11S64') {
                        W11count += 1;
                    } else {
                        console.log('error in reading creeps in W12S63 expedition harvester');
                    }
                }
                
                if (W11count > 0) {
                    newMineSource = 0;
                    mineRoom = 'W12S64';
                } else {
                    newMineSource = 0;
                    mineRoom = 'W11S64';
                }
                
            } else if (roomName == 'W12S64') { 
                // second room
                // 3 harvesters, one to W11S64 source 1, two to W12S65
                
                var W11count = 0,
                    W12count = 0,
                    W12SourceZero = 0;
                
                for (var creepid in exp_harvesters) {
                    if ( exp_harvesters[creepid].memory.destination == 'W12S65' ) {
                        W12count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            W12SourceZero += 1;
                        }
                    } else if ( exp_harvesters[creepid].memory.destination == 'W11S64') {
                        W11count += 1;
                    } else {
                        console.log('error in reading creeps in W12S63 expedition harvester');
                    }
                }
                
                if (W11count == 0) {
                    mineRoom = 'W11S64';
                    newMineSource = 1;
                } else if (W12count == 1 && W12SourceZero == 0) {
                    mineRoom = 'W12S65';
                    newMineSource = 0;
                } else {
                    mineRoom = 'W12S65';
                    newMineSource = 1;
                }
                
            } else if (roomName == 'W11S65') {
                // third room
                // no harvesters
                
                newMineSource = 0;
                mineRoom = '';
                console.log('should not be here');
                
            } else if (roomName == 'W16S67') {
                // fourth room
                // 5 harvesters, two to W15S67, three to W16S66
                
                var W15count = 0,
                    W15SourceZero = 0,
                    W16count = 0,
                    W16SourceZero = 0,
                    W16SourceOne = 0;
                
                for (var creepid in exp_harvesters) {
                    if ( exp_harvesters[creepid].memory.destination == 'W15S67' ) {
                        W15count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            W15SourceZero += 1;
                        }
                    } else if ( exp_harvesters[creepid].memory.destination == 'W16S66') {
                        W16count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            W16SourceZero += 1;
                        } else if (exp_harvesters[creepid].memory.mineSource == 1) {
                            W16SourceOne += 1;
                        }
                    } else {
                        console.log('error in reading creeps in W16S67 expedition harvester');
                    }
                }
                
                if (W15count == 0) {
                    mineRoom = 'W15S67';
                    newMineSource = 0;
                } else if (W15count == 1 && W15SourceZero == 0) {
                    mineRoom = 'W15S67';
                    newMineSource = 0;
                } else if (W15count == 1 && W15SourceZero == 1) {
                    mineRoom = 'W15S67';
                    newMineSource = 1;
                } else if (W16count > 0 && W16SourceZero == 0) {
                    mineRoom = 'W16S66';
                    newMineSource = 0;
                } else if (W16count > 0 && W16SourceOne == 0) {
                    mineRoom = 'W16S66';
                    newMineSource = 1;
                } else {
                    mineRoom = 'W16S66';
                    newMineSource = 2;
                }
                
            } else if (roomName == 'W13S65') {
                // fifth room
                // 3 harvesters to SK room W14S65

                var sourceZero = 0;
                var sourceOne = 0;

                for (var creepid in exp_harvesters) {
                    if (exp_harvesters[creepid].memory.mineSource == 0) {
                        sourceZero += 1;
                    } else if (exp_harvesters[creepid].memory.mineSource == 1) {
                        sourceOne += 1;
                    } else {
                        console.log('error in reading creeps in W13S65 expedition harvester');
                    }
                }

                if (sourceZero == 0) {
                    mineRoom = 'W14S65';
                    newMineSource = 0;
                } else if (sourceOne == 0) {
                    mineRoom = 'W14S65';
                    newMineSource = 1;
                } else {
                    mineRoom = 'W14S65';
                    newMineSource = 2;
                }

            } else if (roomName == 'W15S73') {
                // sixth room
                // 4 to north room, eventually more to south SK room
                var W15count = 0,
                    W15SourceZero = 0,
                    W16count = 0,
                    W16SourceZero = 0,
                    W16SourceOne = 0,
                    S74count = 0,
                    S74SourceZero = 0,
                    S74SourceOne = 0;
                
                for (var creepid in exp_harvesters) {
                    if ( exp_harvesters[creepid].memory.destination == 'W15S72' ) {
                        W15count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            W15SourceZero += 1;
                        }
                    } else if ( exp_harvesters[creepid].memory.destination == 'W16S73') {
                        W16count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            W16SourceZero += 1;
                        } else if (exp_harvesters[creepid].memory.mineSource == 1) {
                            W16SourceOne += 1;
                        }
                    } else if ( exp_harvesters[creepid].memory.destination == 'W15S74') {
                        S74count += 1;
                        if (exp_harvesters[creepid].memory.mineSource == 0) {
                            S74SourceZero += 1;
                        } else if (exp_harvesters[creepid].memory.mineSource == 1) {
                            S74SourceOne += 1;
                        }
                    } else {
                        console.log('error in reading creeps in W15S73 expedition harvester');
                    }
                }
                
                if (W15count == 0) {
                    mineRoom = 'W15S72';
                    newMineSource = 0;
                } else if (W15count == 1 && W15SourceZero == 0) {
                    mineRoom = 'W15S72';
                    newMineSource = 0;
                } else if (W15count == 1 && W15SourceZero == 1) {
                    mineRoom = 'W15S72';
                    newMineSource = 1;
                } else if (S74count == 0) {
                    mineRoom = 'W15S74';
                    newMineSource = 0;
                } else if (S74SourceZero == 0) {
                    mineRoom = 'W15S74';
                    newMineSource = 0;
                } else if (S74SourceOne == 0) {
                    mineRoom = 'W15S74';
                    newMineSource = 1;
                } else if (S74SourceZero == 1 && S74SourceOne == 1) {
                    mineRoom = 'W15S74';
                    newMineSource = 2;
                } else {
                    mineRoom = 'W16S73';
                    newMineSource = 0;
                }
            }
            
            creep.memory.mineSource = newMineSource;
            creep.memory.destination = mineRoom;
        }

		// find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);
        
        if (creep.memory.mineSource == undefined || creep.memory.destination == undefined) {
            setMineInfo();
        }
        
        if (creep.memory.mining == undefined) {
            creep.memory.mining = true;
        }

    	if(creep.carry.energy === 0 && creep.memory.mining == false && creep.hits == creep.hitsMax) {
            // if the creep is carrying less than max energy, go mine
            creep.memory.mining = true;
        } else if (creep.memory.mining == true && (creep.carry.energy == creep.carryCapacity || creep.hits < (.8 * creep.hitsMax) ) ) {
            // if the creep is mining and at full energy, go drop off at a container
            creep.memory.mining = false;
        }
        

    	//if (creep.memory.mining && creep.room.name === creep.memory.room) {
    	if (creep.memory.mining && creep.room.name != creep.memory.destination) {
    		// mining and in spawn room, go to other room
    		var destFlag = "room" + creep.memory.destination;
    		creep.moveTo(Game.flags[destFlag]);
    	} else if (!creep.memory.mining && creep.room.name != creep.memory.room) {
    		// not mining and not in spawn room, try to drop in nearby container
    		// otherwise, go to spawn room
            myFunctions.repairCurrentLocation(creep);
            creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
            /*var nearbyContainer = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_CONTAINER) &&
                    (structure.energyCapcity - structure.energy) >= creep.carryCapacity &&
                    creep.getRangeTo(structure) <= 3;
                }
            });
            if (nearbyContainer.length > 0) {
                if (creep.transfer(nearbyContainer[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(nearbyContainer[0]);
                }
            } else {
    		    creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
            }*/
    	} else if (!creep.memory.mining && creep.room.name == creep.memory.room) {
    		// not mining and in spawn room
    		// Search for containers with enough storage space
            // if there is a valid container, move to it and transfer energy
            // to do: make sure container is within a certain range
            var containers = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER ||
                            structure.structureType == STRUCTURE_CONTAINER ||
                            structure.structureType == STRUCTURE_STORAGE ||
                            structure.structureType == STRUCTURE_LINK) && 
                    		(_.sum(structure.store) < structure.storeCapacity ||
                    		structure.energy < structure.energyCapacity);
                }
            });
            if(containers.length > 0) {
                myFunctions.repairCurrentLocation(creep);
                var target = creep.pos.findClosestByPath(containers);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
            }
    	} else if (creep.memory.mining && creep.room.name == creep.memory.destination) {
    		// mining and in other room
    		var droppedEnergy = creep.pos.findClosestByPath(FIND_DROPPED_ENERGY);
	    	if (droppedEnergy && droppedEnergy.energy >= 700 && creep.pos.getRangeTo(droppedEnergy) < 5) {
	    		//console.log('dropped');
	    		if (creep.pickup(droppedEnergy) == ERR_NOT_IN_RANGE) {
	    			creep.moveTo(droppedEnergy);
	    		}
	    	} else {
                var sources = creep.room.find(FIND_SOURCES);
                //var source = creep.pos.findClosestByPath(sources);
                var source = sources[creep.memory.mineSource];
                try {
                	var harvestResult = creep.harvest(source);
                } catch (err) {
                	console.log('harvest result error: ' + err.message);
                }
                if(harvestResult == ERR_NOT_IN_RANGE || harvestResult == ERR_NOT_ENOUGH_RESOURCES) {
                    creep.moveTo(source);
                } else if (harvestResult == ERR_NOT_ENOUGH_RESOURCES) {
                    creep.memory.mining = false;
                }
	    	}
    	} else {
    		console.log("This should never happen.");
    	}

    }
};

module.exports = roleExpeditionHarvester;