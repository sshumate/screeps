var rooms = require('rooms');
var repController = require('controller.repairer');

var roleRepairer = {

    /** @param {Creep} creep **/
    run: function (creep) {

        const BUILDING_PCT = .66;

        function checkRepair (repairResult) {
          if (repairResult === 0) {
            //console.log("Repair result: (" + repairResult + ") The operation has been scheduled successfully.");
          } else if (repairResult === -1) {
            console.log("Repair result: (" + repairResult + ") You are not the owner of this creep.");
          } else if (repairResult === -4) {
            console.log("Repair result: (" + repairResult + ") You are not the owner of this creep.");
          } else if (repairResult === -6) {
            console.log("Repair result: (" + repairResult + ") The creep does not carry any energy.");
          } else if (repairResult === -7) {
            console.log("Repair result: (" + repairResult + ") The target is not a valid creep object.");
          } else if (repairResult === -9) {
            //console.log("Repair result: (" + repairResult + ")  The target is too far away.");
          } else if (repairResult === -12) {
            console.log("Repair result: (" + repairResult + ") There are no WORK body parts in this creep’s body.");
          } else {
            console.log("Nothing to repair");
          }
        }

        function getEnergy() {
          // look for a storage to get energy from and then retrieve it
          if (creep.room.name != creep.memory.room) {
              creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
          } else {
            var storages = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                }
            });
            if(storages.length > 0) {

                var target = creep.pos.findClosestByPath(storages);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }

            } else {

              // if no storage has energy, search for a container
              var containers = creep.room.find(FIND_STRUCTURES, {
                  filter: (structure) => {
                      return (structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) > creep.carryCapacity;
                  }
              });
              if(containers.length > 0) {
                var target = creep.pos.findClosestByPath(containers);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
              } else {
                  // if no storages and no containers, search main room
                  var mainStorages = Game.rooms[creep.memory.room].find(FIND_STRUCTURES, {
                     filter: (structure) => {
                         return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                     } 
                  });
                  //console.log(mainStorages);
                  if (mainStorages.length > 0) {
                      creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
                  }
              }
            }
          }
        }

        function getNewRepair() {

          var newId = repController.getRepair(creep.room.name);
          //console.log('new id: ' + newId);
          creep.memory.repairID = newId;
          console.log(creep.name + ' setting new repair Id to: ' + creep.memory.repairID);
          
        }

        function repairFromMemory() {

          // we have a thing we are repairing in memory, get that object by the stored ID
          // then move to and repair that item.
          
            var thingToRepair = Game.getObjectById(creep.memory.repairID);

          try {
            var repairRoom = thingToRepair.room.name;
          } catch (err) {
            console.log(creep.name + ' error: ' + err.message);
            creep.memory.repairID = 0;
          }

          if (creep.room.name != repairRoom) {
            creep.moveTo(Game.flags['room' + repairRoom]);
          } else {

            if (thingToRepair && (thingToRepair.structureType === STRUCTURE_WALL || thingToRepair.structureType === STRUCTURE_RAMPART)) {

              if (thingToRepair.structureType === STRUCTURE_WALL && thingToRepair.hits < (rooms.getRoom(repairRoom).WALL_HP) ) {
                
                // if it is a wall, repair it to the amount of HP assigned in rooms.js
                var repairResult = creep.repair(thingToRepair);
                if ( repairResult === ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                checkRepair(repairResult);

              } else if (thingToRepair.structureType === STRUCTURE_WALL && thingToRepair.hits < ( rooms.getRoom(repairRoom).WALL_HP + (.15 * rooms.getRoom(repairRoom).WALL_HP) ) ) {
                // if it is a rampart, repair it to the amount of HP assigned in rooms.js, plus 15%
                var repairResult = creep.repair(thingToRepair);
                if ( repairResult === ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                checkRepair(repairResult);
              } else {

                console.log(creep.name + ' wall is full.' + thingToRepair.structureType + '. Hits: ' + thingToRepair.hits + '/' + thingToRepair.hitsMax);
                creep.memory.repairID = 0;

              }

            } else {

              // if it is anything other than a wall, repair it to full
              if (thingToRepair && thingToRepair.hits < thingToRepair.hitsMax) {

                var repairResult = creep.repair(thingToRepair);
                if ( repairResult === ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                checkRepair(repairResult);

              } else {

                console.log(creep.name + ' non-wall is full. Type: ' + thingToRepair.structureType + '. Hits: ' + thingToRepair.hits + '/' + thingToRepair.hitsMax);
                creep.memory.repairID = 0;

              }

            }
          }
        }

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        var ticksLeft = (creep.hitsMax * .03);
        if (creep.memory.fading == false && creep.ticksToLive < ticksLeft) {
            creep.memory.fading = true;
            console.log(creep.name + ' fading away...');
        }

        if(creep.carry.energy === 0) {

          getEnergy();

        } else if (!creep.memory.repairID || creep.memory.repairID === 0) {

          // if we have energy and there is no item we are currently repairing, find a new one
          getNewRepair();

        } else {

          // we already have something in memory, let's repair it
          repairFromMemory();

        }
    }
};

module.exports = roleRepairer;