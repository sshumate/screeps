var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleRepairer = require('role.repairer');
var roleTrucker = require('role.trucker');
var roleWarrior = require('role.warrior');
var roleExpeditionHarvester = require('role.expeditionHarvester');
//var roleLonghaulTrucker = require('role.longhaulTrucker');
var roleHealer = require('role.healer');
var roleClaimer = require('role.claimer');
var roleLinker = require('role.linker');
//var repController = require('controller.repairer');
var roleGrabber = require('role.grabber');
var roleMiner = require('role.miner');
var roleSkirmisher = require('role.skirmisher');

var myFunctions = require('functions');
var rooms = require('rooms');
var spawnControl = require('controller.spawn');

// to do:
// red alert
// send warriors to an attack automatically (respond to defend or red alert flag)
// spawning code (maybe store it in an external file or at least constant variable ? that might do it)
// store things in memory better
// make repairmen fix ramparts before walls
//      -- also repair ramparts beyond their repair threshold
// ***** allow whitelisted players to pass through ramparts

module.exports.loop = function () {
    
    // TEST AREA
    
    // number of each type of creep to make
    var myRooms = rooms.getRooms();

    // determine if we are in red alert mode
    var redAlert = Memory.redAlert;

    // ------------------------------------ END VARIABLES
    // ------------------------------------ START FUNCTIONS

    function assignCreeps() {
        // assign each creep to its role
        for(var name in Game.creeps) {
            var creep = Game.creeps[name];
            //var startCpu = Game.cpu.getUsed();
            if(creep.memory.role == 'harvester') {
                roleHarvester.run(creep);
            } else if(creep.memory.role == 'upgrader') {
                roleUpgrader.run(creep);
            } else if(creep.memory.role == 'builder') {
                roleBuilder.run(creep);
            } else if(creep.memory.role == 'repairer') {
                roleRepairer.run(creep);
            } else if(creep.memory.role == 'trucker') {
                roleTrucker.run(creep);
            } else if(creep.memory.role == 'warrior') {
                roleWarrior.run(creep);
            } else if(creep.memory.role == 'healer') {
                roleHealer.run(creep);
            } else if(creep.memory.role == 'exp_harvester') {
                roleExpeditionHarvester.run(creep);
            } else if(creep.memory.role == 'claimer') {
                roleClaimer.run(creep);
            } else if(creep.memory.role == 'linker') {
                roleLinker.run(creep);
            } else if(creep.memory.role == 'grabber') {
                roleGrabber.run(creep);
            } else if(creep.memory.role == 'miner') {
                roleMiner.run(creep);
            } else if(creep.memory.role == 'skirmisher') {
                roleSkirmisher.run(creep);
            } else if(creep.memory.role == 'longhaulTrucker') {
                //roleLonghaulTrucker.run(creep);    
            } else {
                console.log(creep.name + ' is ending himself, because he is without purpose.');
                Game.notify(creep.name + ' is ending himself, because he is without purpose.');
                creep.suicide();
            }
            //console.log(creep.memory.role + " used CPU amount: " + (Game.cpu.getUsed() - startCpu));
        }
    }
    
    function runLabs(roomName) {
        var labs = Game.spawns[myRooms[name].spawnName].room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_LAB);
            }
        });
        
    }

    // ------------------------------------ END FUNCTIONS
    // ------------------------------------ BEGIN PROGRAM FUNCTION

    // Clear empty creeps
    spawnControl.clearOldCreeps();

    // spawn new creeps as needed
    for (var name in Game.spawns) {
        try {
            var closestHostile = Game.spawns[name].pos.findClosestByRange(FIND_HOSTILE_CREEPS);
            if (closestHostile) {
                myFunctions.reportEnemy(closestHostile);
            }
        } catch (err) {
            Game.notify('Error in spawn hostile thingy: ' + err.stack);
            console.log('Error in spawn hostile thingy: ' + err.message);
        }
        var spawn = Game.spawns[name];
        if (spawn.spawning) {
            try {
                new RoomVisual(spawn.room.name).text( (spawn.spawning.name + ' ' + spawn.spawning.remainingTime), (spawn.pos.x), (spawn.pos.y + 1.5), {color: 'red', size: 0.65});
            } catch (err) {
                //console.log("name: " + spawn.spawning.name + '(' + spawn.room.name + ')');
                //console.log("time: " + spawn.spawning.remainingTime);
                //console.log(err.message);
            }
        } else {
            spawnControl.manageSpawn(spawn);
        }
    }
    
    // Assign creep roles
    assignCreeps();

    // Run the towers and links in all rooms
    for (var name in myRooms) {
        try {
            if (myRooms[name].spawnName != 'NONE') {
                var towers = Game.spawns[myRooms[name].spawnName].room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_TOWER);
                    }
                });
                
                if (towers.length > 0) {
                    for (var tname in towers) {
                        myFunctions.runTower(towers[tname]);
                    }
                }
                
                myFunctions.runLinks(myRooms[name].roomName);
            }
        } catch (err) {
            //console.log("caught error in running towers and links loops: " + err.message);
        }
        
    }
    
    if (Game.cpu.bucket < 2500) {
        console.log('Bucket low! Value: ' + Game.cpu.bucket);
        Game.notify('Bucket low! Value: ' + Game.cpu.bucket);
    }
    
    // 1. at the first of the hour, run our energy management function
    // 2. report on the status to log every 5 minutes
    // 3. report on the status by email at 7am, 11am, 4pm, and 9pm
    var d = new Date();
    
    if (!Memory.emailRecently && (d.getHours() == 13 || d.getHours() == 17 || d.getHours() == 22 || d.getHours() == 3) && d.getMinutes() == 0 ) {
        myFunctions.emailStatus();
        Memory.emailRecently = true;
    } else if (Memory.emailRecently && (d.getHours() == 13 || d.getHours() == 17 || d.getHours() == 22 || d.getHours() == 3) && d.getMinutes() >= 1) {
        Memory.emailRecently = false;
    } else if (!Memory.reportRecently && (d.getMinutes() % 60) == 0) {
        for (var spawn in Game.spawns) {
            var roomName = Game.spawns[spawn].room.name;
            myFunctions.manageEnergyGain(roomName, 'HOUR', true);
        }
    } else if (!Memory.reportRecently && (d.getMinutes() % 5) == 0 ) {
        myFunctions.reportStatus();
        Memory.reportRecently = true;
    } else if (Memory.reportRecently && (d.getMinutes() % 5) == 1) {
        Memory.reportRecently = false;
    } 
    
    // ------------------------------------ END PROGRAM FUNCTION
}