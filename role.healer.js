var rooms = require('rooms');
var myFunctions = require('functions');

var roleHealer = {

/** @param {Creep} creep **/
    run: function(creep) {
        function heal() {
            if(creep.heal(target) == ERR_NOT_IN_RANGE) {
                if ( target.pos.x < 47 ) {
                    creep.moveTo(target);
                }
            } 
        }
        var myCreeps = creep.room.find(FIND_MY_CREEPS, {
            filter: (creep) => {
                return (creep.hits < creep.hitsMax);
            }
        });
        var target = creep.pos.findClosestByRange(myCreeps);

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);
        
        if(target) {
            heal();
        } else {
        	if (Game.flags.Defend1) {
	            creep.moveTo(Game.flags.Defend1);
        	} else {
            	creep.moveTo(Game.flags.Fight1);
            }
        }
    }
};

module.exports = roleHealer;