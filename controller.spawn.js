// 100% non-functional, just bouncing around an idea
// toughs, works, carries, ranged_attacks, attacks, heals, claims, moves, control_level
var myCreeps = {
    'harvester' : {
            RCL_ONE :         [0,2,1,0,0,0,0,1],        // 2 work, 1 carry, 1 move = 300
            RCL_TWO :         [0,4,2,0,0,0,0,1],        // 4 work, 2 carry, 1 move = 550
            RCL_THREE :       [0,5,3,0,0,0,0,2],        // 5 work, 3 carry, 2 move = 750
            RCL_FOUR_HALF :   [0,6,5,0,0,0,0,4],        // 6 work, 5 carry, 4 move = 800
            RCL_FOUR :        [0,8,6,0,0,0,0,4],        // 8 works, 6 carries, 4 moves = 1100
            RCL_FIVE :        [0,10,10,0,0,0,0,6],      // 10 works, 10 carries, 6 moves = 1800
            RCL_SIX :         [0,10,10,0,0,0,0,6],      // 10 works, 10 carries, 6 moves = 1800
            RCL_SEVEN :       [0,10,10,0,0,0,0,6],      // 10 works, 10 carries, 6 moves = 1800 *not max*
            RCL_EIGHT :       [0,10,10,0,0,0,0,6]       // 10 works, 10 carries, 6 moves = 1800 *not max*
        },
    'trucker' : {
            RCL_ONE :         [0,0,3,0,0,0,0,3],          // 3 carry, 3 move = 300
            RCL_TWO :         [0,0,5,0,0,0,0,5],          // 5 carry, 5 move = 500
            RCL_THREE :       [0,0,8,0,0,0,0,8],          // 8 carry, 8 move = 800
            RCL_FOUR_HALF :   [0,0,7,0,0,0,0,8],          // 7 carry, 8 move = 750
            RCL_FOUR :        [0,0,10,0,0,0,0,10],        // 10 carry, 10 move = 1000
            RCL_FIVE :        [0,0,10,0,0,0,0,10],        // 10 carry, 10 move = 1000
            RCL_SIX :         [0,0,20,0,0,0,0,20],        // 20 carry, 20 move = 2000
            RCL_SEVEN :       [0,0,20,0,0,0,0,20],        // 20 carry, 20 move = 2000 *not max*
            RCL_EIGHT :       [0,0,20,0,0,0,0,20]         // 20 carry, 20 move = 2000 *not max*
        },
    'upgrader' : {
            RCL_ONE :         [0,1,1,0,0,0,0,1],            // 1 work, 1 carry, 1 move = 200
            RCL_TWO :         [0,2,3,0,0,0,0,2],            // 2 work, 3 carry, 2 move = 450
            RCL_THREE :       [0,5,4,0,0,0,0,2],            // 5 work, 4 carry, 2 move = 800
            RCL_FOUR_HALF :   [0,3,4,0,0,0,0,2],            // 3 work, 4 carry, 2 move = 600        review
            RCL_FOUR :        [0,7,6,0,0,0,0,5],            // 7 work, 6 carry, 5 move = 1250       review
            RCL_FIVE :        [0,10,6,0,0,0,0,10],          // 10 work, 6 carry, 10 move = 1800
            RCL_SIX :         [0,13,10,0,0,0,0,10],         // 13 work, 10 carry, 10 move = 2300
            RCL_SEVEN :       [0,13,10,0,0,0,0,10],         // 13 work, 10 carry, 10 move = 2300 *not max*
            RCL_EIGHT :       [0,15,10,0,0,0,0,8]           // 15 work, 10 carry, 8 move = 2400 *not max but efficient*
        },
    'builder' : {
            RCL_ONE :         [0,1,1,0,0,0,0,1],              // 1 work, 1 carry, 1 move = 200
            RCL_TWO :         [0,2,2,0,0,0,0,2],              // 2 work, 2 carry, 2 move = 400
            RCL_THREE :       [0,2,4,0,0,0,0,3],              // 2 work, 4 carry, 3 move = 550
            RCL_FOUR_HALF :   [0,2,4,0,0,0,0,3],              // 2 work, 4 carry, 3 move = 550
            RCL_FOUR :        [0,7,6,0,0,0,0,5],              // 7 work, 6 carry, 5 move = 1250
            RCL_FIVE :        [0,5,10,0,0,0,0,15],            // 7 work, 6 carry, 5 move = 1250      ????
            RCL_SIX :         [0,5,10,0,0,0,0,15],            // 5 work, 10 carry, 15 move = 1750
            RCL_SEVEN :       [0,5,10,0,0,0,0,15],            // 5 work, 10 carry, 15 move = 1750 *not max*
            RCL_EIGHT :       [0,5,10,0,0,0,0,15]             // 5 work, 10 carry, 15 move = 1750 *not max*
        },
    'repairer' : {
            RCL_ONE :         [0,1,1,0,0,0,0,1],               // 1 work, 1 carry, 1 move = 200
            RCL_TWO :         [0,3,3,0,0,0,0,2],               // 3 work, 3 carry, 2 move = 550
            RCL_THREE :       [0,4,4,0,0,0,0,1],               // 4 work, 4 carry, 1 move = 650
            RCL_FOUR_HALF :   [0,4,4,0,0,0,0,1],               // 4 work, 4 carry, 1 move = 650
            RCL_FOUR :        [0,7,7,0,0,0,0,4],               // 7 work, 7 carry, 4 move = 1250
            RCL_FIVE :        [0,7,7,0,0,0,0,4],               // 7 work, 7 carry, 4 move = 1250
            RCL_SIX :         [0,9,9,0,0,0,0,18],              // 9 work, 9 carry, 18 move = 1250
            RCL_SEVEN :       [0,9,9,0,0,0,0,18],              // 9 work, 9 carry, 18 move = 1250
            RCL_EIGHT :       [0,9,9,0,0,0,0,18]              // 9 work, 9 carry, 18 move = 1250
        },
    'warrior' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR_HALF :   [],
            RCL_FOUR :        [13,0,0,4,5,0,0,3],              // 13 tough (130), 4 ranged attack (600), 5 attack (400), 3 move (150) = 1280
            RCL_FIVE :        [13,0,0,4,5,0,0,13],             // 13 tough (130), 4 ranged attack (600), 5 attack (400), 13 move (650) = 1780
            RCL_SIX :         [13,0,0,4,5,0,0,13],             // 13 tough (130), 4 ranged attack (600), 5 attack (400), 13 move (650) = 1780 *not max*
            RCL_SEVEN :       [10,0,0,4,10,1,0,25],            // 10 tough (100), 4 ranged attack (600), 10 attack (800), 1 heal (250), 25 move (1250) = 3000
            RCL_EIGHT :       [10,0,0,4,10,1,0,25]             // 10 tough (100), 4 ranged attack (600), 10 attack (800), 1 heal (250), 25 move (1250) = 3000 *not max*
        },
    'healer' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR_HALF :   [],
            RCL_FOUR :        [25,0,0,0,0,2,0,10],               // 25 tough, 10 move, 2 heal = 1250
            RCL_FIVE :        [25,0,0,0,0,2,0,10],               // 25 tough, 10 move, 2 heal = 1250 *not max*
            RCL_SIX :         [25,0,0,0,0,2,0,10],               // 25 tough, 10 move, 2 heal = 1250 *not max*
            RCL_SEVEN :       [25,0,0,0,0,2,0,10],               // 25 tough, 10 move, 2 heal = 1250 *not max*
            RCL_EIGHT :       [25,0,0,0,0,2,0,10]                // 25 tough, 10 move, 2 heal = 1250 *not max*
        },
    'exp_harvester' : {
            RCL_ONE :         [0,1,1,0,0,0,0,1],                 // 1 work, 1 carry, 1 move = 200
            RCL_TWO :         [0,3,2,0,0,0,0,2],                  // 3 work, 2 carry, 2 move = 500
            RCL_THREE :       [0,5,3,0,0,0,0,2],                  // 5 work, 3 carry, 2 move = 750
            RCL_FOUR_HALF :   [0,6,5,0,0,0,0,4],                  // 6 work, 5 carry, 4 move = 1050
            RCL_FOUR :        [0,8,6,0,0,0,0,4],                  // 8 work, 6 carry, 4 move = 1300
            RCL_FIVE :        [0,10,6,0,0,0,0,10],                // 10 work, 6 carry, 10 move = 1800
            RCL_SIX :         [0,10,8,0,0,0,0,18],                // 10 work, 8 carry, 18 move = 2300
            RCL_SEVEN :       [0,15,20,0,0,0,0,15],               // 15 work (1500), 20 carry (1000), 15 move (750) = 3250
            RCL_EIGHT :       [0,15,20,0,0,0,0,15]                // 15 work (1500), 20 carry (1000), 15 move (750) = 3250 *not max*
        },
    'claimer' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR_HALF :   [],
            RCL_FOUR :        [0,0,0,0,0,0,2,2],                  // 2 claim (1200), 2 move (100) = 1300
            RCL_FIVE :        [8,0,0,0,0,0,2,10],                 // 8 tough (80), 2 claim (1200), 10 move (500) = 1780
            RCL_SIX :         [8,0,0,0,0,0,2,10],                 // 8 tough (80), 2 claim (1200), 10 move (500) = 1780 *not max*
            RCL_SEVEN :       [8,0,0,0,0,0,2,10],                 // 8 tough (80), 2 claim (1200), 10 move (500) = 1780 *not max*
            RCL_EIGHT :       [8,0,0,0,0,0,2,10]                  // 8 tough (80), 2 claim (1200), 10 move (500) = 1780 *not max*
        },
    'linker' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR :        [],
            RCL_FOUR_HALF :   [],
            RCL_FIVE :        [0,0,8,0,0,0,0,5],                  // 8 carry, 5 move = 650
            RCL_SIX :         [],
            RCL_SEVEN :       [],
            RCL_EIGHT :       []
        },
    'skirmisher' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR :        [],
            RCL_FOUR_HALF :   [],                
            RCL_FIVE :        [],                  
            RCL_SIX :         [],
            RCL_SEVEN :       [0,0,0,0,15,10,0,25],
            RCL_EIGHT :       [0,0,0,0,15,10,0,25]
        },
    'miner' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR :        [],
            RCL_FOUR_HALF :   [],
            RCL_FIVE :        [],                  
            RCL_SIX :         [0,10,6,0,0,0,0,16],              // 10 work (1000), 6 carry (300), 16 move (900) = 2100
            RCL_SEVEN :       [0,12,6,0,0,0,0,18],              // 12 work (1200), 6 carry (300), 18 move (900) = 2400
            RCL_EIGHT :       [0,12,6,0,0,0,0,18],              // 12 work (1200), 6 carry (300), 18 move (900) = 2400 *not max*
    },
    'grabber' : {
            RCL_ONE :         [],
            RCL_TWO :         [],
            RCL_THREE :       [],
            RCL_FOUR :        [],
            RCL_FOUR_HALF :   [],
            RCL_FIVE :        [],
            RCL_SIX :         [0,7,10,0,0,0,0,18],                // 7 work, 10 carry, 18 move
            RCL_SEVEN :       [0,7,10,0,0,0,1,18],                // 7 work, 10 carry, 1 claim, 18 move
            RCL_EIGHT :       []
    }
};

// ----- energy levels at max extenders for each level of room control
const EN_RCL_EIGHT = 12000;    // 60 extensions (now 200 energy each)
const EN_RCL_SEVEN = 5000;     // 50 extensions (now 100 energy each)
const EN_RCL_SIX = 2300;       // 40 extensions
const EN_RCL_FIVE = 1800;      // 30 extensions
const EN_RCL_FOUR = 1300;      // 20 extensions
const EN_RCL_FOUR_HALF = 1050; // 15 extensions
const EN_RCL_THREE = 800;      // 10 extensions
const EN_RCL_TWO = 550;        // 5 extensions
const EN_RCL_ONE = 300;        // 0 extensions

var rooms = require('rooms');

var myCreepsFunc = {
    checkName: function(newName, type) {
        // this function checks error codes for the spawning function and reports for certain returned values
        if ( newName === -1 ) {
            console.log('Could not spawn ' + type + '. Error -1: you are not the owner of this spawn.');
        } else if ( newName === -3 ) {
            console.log('Could not spawn ' + type + '. Error -3: There is a creep with the same name already.');
        } else if ( newName === -4 ) {
            console.log('Could not spawn ' + type + '. Error -4: The spawn is already in process of spawning another creep.');
        } else if ( newName === -6 ) {
            //console.log('Could not spawn ' + type + '. Error -6: The spawn and its extensions contain not enough energy to create a creep with the given body.');
        } else if ( newName === -10 ) {
            console.log('Could not spawn ' + type + '. Error -10: Body is not properly described.');
        } else if ( newName === -14 ) {
            console.log('Could not spawn ' + type + '. Error -14: Your Room Controller level is insufficient to use this spawn.');
        } else {
            console.log('Spawning new ' + type + ': ' + newName);
        }
    },

    getNewName: function(creeps, type) {
        // this function generates a name for a creep of a given type with the format "<given name> the <role>""
        // ex: Jackson the Harvester
        // It should not produce duplicates, but for some reason it does sometimes right now, and this is usually fixed on the next tick.
        var names1 = ["Jackson", "Aiden", "Liam", "Lucas", "Noah", "Mason", "Jayden", "Ethan", "Jacob", "Jack", "Caden", "Logan", "Benjamin", "Michael", "Caleb", "Ryan", "Alexander", "Elijah", "James", "William", "Oliver", "Connor", "Matthew", "Daniel", "Luke", "Brayden", "Jayce", "Henry", "Carter", "Dylan", "Gabriel", "Joshua", "Nicholas", "Isaac", "Owen", "Nathan", "Grayson", "Eli", "Landon", "Andrew", "Max", "Samuel", "Gavin", "Wyatt", "Christian", "Hunter", "Cameron", "Evan", "Charlie", "David", "Sebastian", "Joseph", "Dominic", "Anthony", "Colton", "John", "Tyler", "Zachary", "Thomas", "Julian", "Levi", "Adam", "Isaiah", "Alex", "Aaron", "Parker", "Cooper", "Miles", "Chase", "Muhammad", "Christopher", "Blake", "Austin", "Jordan", "Leo", "Jonathan", "Adrian", "Colin", "Hudson", "Ian", "Xavier", "Camden", "Tristan", "Carson", "Jason", "Nolan", "Riley", "Lincoln", "Brody", "Bentley", "Nathaniel", "Josiah", "Declan", "Jake", "Asher", "Jeremiah", "Cole", "Mateo", "Micah", "Elliot"];
        var names2 = ["Sophia", "Emma", "Olivia", "Isabella", "Mia", "Ava", "Lily", "Zoe", "Emily", "Chloe", "Layla", "Madison", "Madelyn", "Abigail", "Aubrey", "Charlotte", "Amelia", "Ella", "Kaylee", "Avery", "Aaliyah", "Hailey", "Hannah", "Addison", "Riley", "Harper", "Aria", "Arianna", "Mackenzie", "Lila", "Evelyn", "Adalyn", "Grace", "Brooklyn", "Ellie", "Anna", "Kaitlyn", "Isabelle", "Sophie", "Scarlett", "Natalie", "Leah", "Sarah", "Nora", "Mila", "Elizabeth", "Lillian", "Kylie", "Audrey", "Lucy", "Maya", "Annabelle", "Makayla", "Gabriella", "Elena", "Victoria", "Claire", "Savannah", "Peyton", "Maria", "Alaina", "Kennedy", "Stella", "Liliana", "Allison", "Samantha", "Keira", "Alyssa", "Reagan", "Molly", "Alexandra", "Violet", "Charlie", "Julia", "Sadie", "Ruby", "Eva", "Alice", "Eliana", "Taylor", "Callie", "Penelope", "Camilla", "Bailey", "Kaelyn", "Alexis", "Kayla", "Katherine", "Sydney", "Lauren", "Jasmine", "London", "Bella", "Adeline", "Caroline", "Vivian", "Juliana", "Gianna", "Skyler", "Jordyn"];
        var name, isNameTaken, tries = 0;

        do {
            var nameArray = Math.random() > .5 ? names1 : names2;
            name = nameArray[Math.floor(Math.random() * nameArray.length)];

            if (type === 'exp_harvester') {
                type = 'Journeyman';
            } else {
                type = type.charAt(0).toUpperCase() + type.slice(1);
            }

            name += ' the ' + type;

            if (tries > 3) {
                name += ', Jr.';
            }

            tries++;
            isNameTaken = Game.creeps[name] !== undefined;
        } while (isNameTaken);

        return name;
    },

    makeBody: function (body_structure, control_level, movesFirst) {
        // this function generates a body based on an input
        // * body_structure is an array of integers in the following order by body part:
        //      toughs, works, carries, ranged_attacks, attacks, heals, claims, moves, control_level
        // * control_level is the RCL of the room for comparison purposes
        // * movesFirst is a boolean value that tells whether or not to put the moves before or after the rest of the body parts

        // bodypart costs
        const BODY_MOVE = 50,
              BODY_WORK = 100,
              BODY_ATTACK = 80,
              BODY_CARRY = 50,
              BODY_HEAL = 250,
              BODY_RANGED_ATTACK = 150,
              BODY_TOUGH = 10,
              BODY_CLAIM = 600;

        //console.log(JSON.stringify(body_structure));

        var toughs = body_structure[0],
            works = body_structure[1],
            carries = body_structure[2],
            ranged_attacks = body_structure[3],
            attacks = body_structure[4],
            heals = body_structure[5],
            claims = body_structure[6],
            moves = body_structure[7];

        var creepValue = 0;
        var output = new Array();
        var count = 0;
        
        if (movesFirst) {
            for (i = 1; i <= moves; i++) {
                output[count] = "move";
                creepValue += BODY_MOVE;
                count += 1;
            }
        }
        for (i = 1; i <= toughs; i++) {
            output[count] = "tough";
            creepValue += BODY_TOUGH;
            count += 1;
        }
        for (i = 1; i <= works; i++) {
            output[count] = "work";
            creepValue += BODY_WORK;
            count += 1;
        }
        for (i = 1; i <= carries; i++) {
            output[count] = "carry";
            creepValue += BODY_CARRY;
            count += 1;
        }
        for (i = 1; i <= ranged_attacks; i++) {
            output[count] = "ranged_attack";
            creepValue += BODY_RANGED_ATTACK;
            count += 1;
        }
        for (i = 1; i <= attacks; i++) {
            output[count] = "attack";
            creepValue += BODY_ATTACK;
            count += 1;
        }
        for (i = 1; i <= heals; i++) {
            output[count] = "heal";
            creepValue += BODY_HEAL;
            count += 1;
        }
        for (i = 1; i <= claims; i++) {
            output[count] = "claim";
            creepValue += BODY_CLAIM;
            count += 1;
        }
        if (!movesFirst) {
            for (i = 1; i <= moves; i++) {
                output[count] = "move";
                creepValue += BODY_MOVE;
                count += 1;
            }
        }
        
        if (creepValue < control_level) {
            //console.log("Inefficient! Control level: " + control_level + ", but only value: " + creepValue);
        } else if (creepValue > control_level) {
            console.log("Over control level! Creep requested with " + creepValue + " energy but only " + control_level + " energy available.");
            Game.notify("Over control level! Creep requested with " + creepValue + " energy but only " + control_level + " energy available.");
        }
        //console.log(output.toString() + " | Value: " + creepValue + " | Length: " + output.length);
        //var spawnTime = (count+1) * 3; // the amount of time it takes to spawn this guy
        return output;
    },

    spawnCreep: function(spawn, role) {
        // 
        var roomName = spawn.room.name;
        var room = rooms.getRoom(roomName);
        var spawnName = room.spawnName;
        var allCreeps = _.filter(Game.creeps, (creep) => creep.memory.role == role);
        var currentRoomCreeps = _.filter(Game.creeps, (creep) => { return creep.memory.role == role && creep.memory.room == roomName && creep.memory.fading == false });
        var cancelSpawn = false;
        var energyCapacityAvailable = Game.spawns[spawnName].room.energyCapacityAvailable;
        var movesFirst;

        if (role == 'skirmisher') { movesFirst = true; } else { movesFirst = false; }

        // make our body from our RCL
        if (energyCapacityAvailable >= EN_RCL_EIGHT)      { var body = this.makeBody(myCreeps[role].RCL_EIGHT, EN_RCL_EIGHT, movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_SEVEN) { var body = this.makeBody(myCreeps[role].RCL_SEVEN, EN_RCL_SEVEN, movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_SIX)   { var body = this.makeBody(myCreeps[role].RCL_SIX,   EN_RCL_SIX,   movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_FIVE)  { var body = this.makeBody(myCreeps[role].RCL_FIVE,  EN_RCL_FIVE,  movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_FOUR)  { var body = this.makeBody(myCreeps[role].RCL_FOUR,  EN_RCL_FOUR,  movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_THREE) { var body = this.makeBody(myCreeps[role].RCL_THREE, EN_RCL_THREE, movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_TWO)   { var body = this.makeBody(myCreeps[role].RCL_TWO,   EN_RCL_TWO,   movesFirst );  }
        else if (energyCapacityAvailable >= EN_RCL_ONE)   { var body = this.makeBody(myCreeps[role].RCL_ONE,   EN_RCL_ONE,   movesFirst );  }

        if (body == undefined || body == []) {
            Game.notify('Body was empty: ' + roomName + ' ' + role);
            console.log('Body was empty: ' + roomName + ' ' + role);
        }

        // make our name from our role
        var addName = this.getNewName(allCreeps, role);
        
        var creepMemory = {};
        creepMemory.role = role;
        creepMemory.room = roomName;
        creepMemory.fading = false;
        
        var newName = spawn.createCreep(body, addName, creepMemory);
        this.checkName(newName, role);
        
    },

    manageSpawn: function(spawn) {
        // 
        var myRooms = rooms.getRooms();
        var room = spawn.room.name;
        for (var role in myCreeps) {
            var spawnName = myRooms[room].spawnName;
            var currentRoomCreeps = _.filter(Game.creeps, (creep) => { return creep.memory.role == role && creep.memory.room == room });
            var numSearch = 'NUM_' + role.toUpperCase() + 'S';
            var currentRoomCapacity = rooms.getRoom(room)[numSearch];
            var needSpawn;

            if (role == 'miner') {
                // if we don't have enough miners AND there are minerals to be mined, spawn a miner
                if ( currentRoomCreeps.length < currentRoomCapacity && Game.spawns[spawnName].room.find(FIND_MINERALS)[0].mineralAmount > 0 ) {
                    needSpawn = true;
                } else {
                    needSpawn = false;
                }

            } else if (role == 'builder') {
                // if we don't have enough miners AND there are things to be built in the room, spawn a builder
                //console.log('sites: ' + (Game.spawns[spawnName].room.find(FIND_CONSTRUCTION_SITES)).length);
                if ( currentRoomCreeps.length < currentRoomCapacity && (Game.spawns[spawnName].room.find(FIND_CONSTRUCTION_SITES)).length > 0 ) {
                    needSpawn = true;
                } else {
                    needSpawn = false;
                }
            } else if (currentRoomCreeps.length < currentRoomCapacity) {
                // for all other creeps, if we don't have enough, spawn them
                needSpawn = true;
            } else {
                // otherwise, don't spawn
                needSpawn = false;
            }

            if (needSpawn) {
                this.spawnCreep(spawn, role);
            }
        }
    },

    clearOldCreeps: function() {
        // commit the memory of old creeps to oblivion to save us resources
        for(var name in Memory.creeps) {
            if(!Game.creeps[name]) {
                delete Memory.creeps[name];
            }
        }
    }
};

module.exports = myCreepsFunc;