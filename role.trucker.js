var rooms = require('rooms');
var myFunctions = require('functions');

var roleTrucker = {

	run: function(creep) {

		var otherTruckers = _.filter(Game.creeps, (trucker) => { return trucker.memory.role == 'trucker' && trucker.memory.room == creep.memory.room &&  trucker.id != creep.id});
		//console.log(JSON.stringify(otherTruckers));
		
		function visibleMove(target, enableVisuals) {
		    if (enableVisuals) {
    		    return creep.moveTo(target,  {  visualizePathStyle: {
                                                fill: 'transparent',
                                                stroke: '#fff',
                                                lineStyle: 'dashed',
                                                strokeWidth: .15,
                                                opacity: .25}});
		    } else {
		        return creep.moveTo(target);
		    }
		}

		// ------------------------------------ BEGIN FUNCTIONS

		function deliverToSpawn() {
			var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION ||
                            structure.structureType == STRUCTURE_SPAWN) && structure.energy < structure.energyCapacity;
                }
            });

            if(targets.length > 0) {

                var target = creep.pos.findClosestByPath(targets);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    visibleMove(target, 1);
                }
                return true;
            } else {
            	return false;
            }
		}

		function deliverToTower() {
			// deliver energy to tower(s)
        	var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                }
            });

            if(targets.length > 0) {
                var target = creep.pos.findClosestByPath(targets);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    visibleMove(target, 1);
                }
                return true;
            } else {
            	return false;
            }
		}
		
		function deliverToContainers() {
		    // find the dropoffs in our room, check if they are full, and if not, fill them up
		    var dropoffs = rooms.getDropoffsByRoom(creep.room.name);
		    var containers = creep.room.find(FIND_STRUCTURES, {
	              filter: (structure) => {
	                  return (structure.structureType == STRUCTURE_CONTAINER) &&
	                         (_.sum(structure.store) < structure.storeCapacity) &&
	                         (dropoffs.indexOf(structure.id) >= 0);
	              }
	            });
	            
	        if ( containers.length > 0 ) {
        		var container = creep.pos.findClosestByPath(containers);
        		var transferResult = creep.transfer(container, RESOURCE_ENERGY);
                if(transferResult == ERR_NOT_IN_RANGE) {
                    var moveResult = visibleMove(container, 1);
                }
                return true;
        	} else {
        		return false;
        	}
		}

        function deliverToTerminal() {
            var storage = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_STORAGE);
              }
            });
            var terminal = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_TERMINAL);
              }
            });

            if ( storage.length == 0 || terminal.length == 0 ) {
                return false;
            } else {
                if ( _.sum(storage[0].store) > (.6 * storage[0].storeCapacity) && _.sum(terminal[0].store) < (.75 * terminal[0].storeCapacity) ) {
                    var transferResult = creep.transfer(terminal[0], RESOURCE_ENERGY);
                    if ( transferResult == ERR_NOT_IN_RANGE ) {
                        var moveResult = visibleMove(terminal[0], 1);
                    }
                    return true;
                } else {
                    return false;
                }
            }
        }

		function deliverToStorage() {
			// deliver energy to storage(s)
        	// for room W12S64, put stuff into other containers
        	var storages = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) < structure.storeCapacity;
              }
            });

        	if ( storages.length > 0 ) {
        		var storage = creep.pos.findClosestByPath(storages);
        		var transferResult = creep.transfer(storage, RESOURCE_ENERGY);
                if(transferResult == ERR_NOT_IN_RANGE) {
                    var moveResult = visibleMove(storage, 1);
                }
                return true;
        	} else {
        		return false;
        	}
		}

        function deliverToLabs() {
            // deliver energy to labs
            var labs = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_LAB) && structure.energy < structure.energyCapacity;
              }
            });

            if ( labs.length > 0 ) {
                var lab = creep.pos.findClosestByPath(labs);
                var transferResult = creep.transfer(lab, RESOURCE_ENERGY);
                if(transferResult == ERR_NOT_IN_RANGE) {
                    var moveResult = visibleMove(lab, 1);
                }
                return true;
            } else {
                return false;
            }
        }

        function deliverToNuker() {
            // deliver energy to nuker
            var nukers = creep.room.find(FIND_STRUCTURES, {
              filter: (structure) => {
                  return (structure.structureType == STRUCTURE_NUKER) && structure.energy < structure.energyCapacity;
              }
            });

            if ( nukers.length > 0 ) {
                var nuker = creep.pos.findClosestByPath(nukers);
                var transferResult = creep.transfer(nuker, RESOURCE_ENERGY);
                if(transferResult == ERR_NOT_IN_RANGE) {
                    var moveResult = visibleMove(nuker, 1);
                }
                return true;
            } else {
                return false;
            }
        }
		
		function getDroppedEnergy() {
			// search for dropped energy, pick it up if it exists, or return false
			if (otherTruckers.length > 0) {
    		    var otherTruckerTargets = new Array();
                for (i = 0; i < otherTruckers.length; i++) {
                    otherTruckerTargets[i] = otherTruckers[i].memory.pickupTarget;
                }
                var droppedEnergy = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES, {
                    filter: (groundEnergy) => {
                        return otherTruckerTargets.indexOf(groundEnergy.id) < 0;
                    }
                });
    	    	if (droppedEnergy && droppedEnergy.energy > 200) {
    	    	    try {
                		creep.memory.pickupTarget = droppedEnergy.id; // maybe remove later
	                } catch (err) {
	                	console.log("Error in trucker assigning dropped energy pickupTarget: " + err.message);
	                }
    	    		if (creep.pickup(droppedEnergy) == ERR_NOT_IN_RANGE) {
    	    			visibleMove(droppedEnergy, 1);
    	    		}
    	    		return true;
    	    	} else {
    	    	    return false;
    	    	}
			} else {
			    var droppedEnergy = creep.pos.findClosestByPath(FIND_DROPPED_RESOURCES);
			    if (droppedEnergy && droppedEnergy.energy > 200) {
			        try {
                		creep.memory.pickupTarget = target.id; // maybe remove later
	                } catch (err) {
	                	//console.log("Error in trucker assigning pickupTarget: " + err.message);
	                }
    	    		if (creep.pickup(droppedEnergy) == ERR_NOT_IN_RANGE) {
    	    			visibleMove(droppedEnergy, 1);
    	    		}
    	    		return true;
    	    	} else {
    	    	    return false;
    	    	}
			}
		}

		function getStoredEnergy() {
    		// get container energy
			if (otherTruckers.length > 0) {
			    // if there are other truckers, select only designated containers they are not headed toward
			    var otherTruckerTargets = new Array();
                for (i = 0; i < otherTruckers.length; i++) {
                    otherTruckerTargets[i] = otherTruckers[i].memory.pickupTarget;
                }
                var pickups = rooms.getPickupsByRoom(creep.room.name);
		    	var containers = creep.room.find(FIND_STRUCTURES, {
	              filter: (structure) => {
	                  return (structure.structureType == STRUCTURE_TOWER ||
	                          structure.structureType == STRUCTURE_CONTAINER ||
	                          structure.structureType == STRUCTURE_LINK) &&
	                  		  otherTruckerTargets.indexOf(structure.id) < 0 &&
	                  		  pickups.indexOf(structure.id) >= 0 &&
	                  		  (_.sum(structure.store) >= creep.carryCapacity ||
	                  		  structure.energy >= creep.carryCapacity);
	              }
	            });
	    	} else {
			    // otherwise, just go to the nearest container designated for pickup
	    	    var pickups = rooms.getPickupsByRoom(creep.room.name);
		    	var containers = creep.room.find(FIND_STRUCTURES, {
	              filter: (structure) => {
	                  return (structure.structureType == STRUCTURE_TOWER ||
	                          structure.structureType == STRUCTURE_CONTAINER) &&
	                          pickups.indexOf(structure.id) >= 0 &&
	                          (_.sum(structure.store) > creep.carryCapacity ||
	                          structure.energy >= creep.carryCapacity);
	              }
	            });
		    }

            if (containers.length == 0 && Game.spawns[rooms.getRoom(creep.room.name).spawnName].room.energyAvailable < Game.spawns[rooms.getRoom(creep.room.name).spawnName].room.energyCapacityAvailable) {
            	// if no containers and spawn needs energy, get storage energy
            	var storages = creep.room.find(FIND_STRUCTURES, {
	              filter: (structure) => {
	                  return (structure.structureType == STRUCTURE_STORAGE && structure.store[RESOURCE_ENERGY] > creep.carryCapacity);
	              }
	            });
            	if ( storages.length > 0 ) {
            		var storage = creep.pos.findClosestByPath(storages);
            		try {
                		creep.memory.pickupTarget = target.id; // maybe remove later
	                } catch (err) {
	                	//console.log("Error in trucker assigning pickupTarget: " + err.message);
	                }
	                if(creep.withdraw(storage, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
	                    visibleMove(storage, 1);
	                }
            	} else {
            		var terminals = creep.room.find(FIND_STRUCTURES, {
        	           filter: (structure) => {
        	               return (structure.structureType == STRUCTURE_TERMINAL && structure.store[RESOURCE_ENERGY] > creep.carryCapacity);
        	           }
    	            });
    	            if (terminals.length > 0) {
    	                var terminal = creep.pos.findClosestByPath(terminals);
    	                if(creep.withdraw(terminal, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
    	                    visibleMove(terminal, 1);
    	                }
    	            } else { 
            		    creep.moveTo(creep.memory.idleX, creep.memory.idleY, {visualizePathStyle: {
                                                                            fill: 'transparent',
                                                                            stroke: '#fff',
                                                                            lineStyle: 'dashed',
                                                                            strokeWidth: .15,
                                                                            opacity: .25}});
            		}
            	}
            } else if (containers.length > 0) {
                // otherwise, go get the energy from the container
                var target = creep.pos.findClosestByPath(containers);
                try {
                	creep.memory.pickupTarget = target.id; // maybe remove later
                } catch (err) {
                	//console.log("Error in trucker assigning pickupTarget: " + err.message);
                }
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    visibleMove(target, 1);
                }
            } else {
                // if none of this needs doing, go to idle location and wait for more
            	creep.moveTo(creep.memory.idleX, creep.memory.idleY, {visualizePathStyle: {
                                                                        fill: 'transparent',
                                                                        stroke: '#fff',
                                                                        lineStyle: 'dashed',
                                                                        strokeWidth: .15,
                                                                        opacity: .25}});
            }
        }

        function getEnergyFromMemory() {
        	if (creep.memory.pickupTarget && creep.memory.pickupTarget != 0) {
        		var target = Game.getObjectById(creep.memory.pickupTarget);
        		if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    visibleMove(target, 1);
                }
        	}
        }
        
        function setIdle() {
			newLoc = rooms.getIdleLocation(creep.room.name);
			creep.memory.idleX = newLoc['x'];
            creep.memory.idleY = newLoc['y'];
        }

		// ------------------------------------ END FUNCTIONS
    	// ------------------------------------ BEGIN DELIVERY STATUS
    	
    	if (creep.memory.idleX == undefined || creep.memory.idleY == undefined) {
    	    setIdle();
    	}
    	
    	if (creep.memory.delivering == undefined) {
    	    creep.memory.delivering = false;
    	}

		if(creep.memory.delivering && creep.carry.energy == 0) {
	        creep.memory.delivering = false;
	        creep.say('Get stuff.');
	    }
	    if(!creep.memory.delivering && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.delivering = true;
	        creep.memory.pickupTarget = 0;
	        creep.say('Give stuff.');
	    }

	    // ------------------------------------ END DELIVERY STATUS
    	// ------------------------------------ BEGIN PROGRAM LOGIC

    	// find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);

	    if (creep.room.name != creep.memory.room) {
	    	visibleMove(Game.spawns[rooms.getRoom(creep.memory.room).spawnName], 1);
	    } else if (creep.memory.delivering) {
	    	// deliver energy to spawn
	    	if ( !deliverToSpawn() ) {
	    		if ( !deliverToTower() ) {
	    		    if ( !deliverToContainers() ) {
                        if ( !deliverToTerminal() ) {
    	    			    if ( !deliverToLabs() ) {
                                if ( !deliverToStorage() ) {
                                    if ( !deliverToNuker() ) {
            	    					//console.log("Well, shit.");
            	    					creep.say("Well shit.");
            	            			creep.moveTo(creep.memory.idleX, creep.memory.idleY, {visualizePathStyle: {
                                                                                                fill: 'transparent',
                                                                                                stroke: '#fff',
                                                                                                lineStyle: 'dashed',
                                                                                                strokeWidth: .15,
                                                                                                opacity: .25}});
                                    }
                                }
    	    			    }
                        }
	    			}
	    		}
            } 
	    } else {
	        if (!getDroppedEnergy() ) {
	            if (!getEnergyFromMemory()) {
	            	getStoredEnergy();
	            }
	        }
	    }
    }
};

module.exports = roleTrucker;