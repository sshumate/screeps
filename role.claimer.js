var rooms = require('rooms');
var myFunctions = require('functions');

var roleClaimer = {

    /** @param {Creep} creep **/
    run: function(creep) {
        
        function setClaim() {
            var roomName = creep.memory.room;
            if (roomName == 'W12S63') {
                newClaimRoom = 'W11S64';
                newClaimFlag = 'Claim1';
            }  else if (roomName == 'W12S64') {
                newClaimRoom = 'W12S65';
                newClaimFlag = 'Claim3';
            } else if (roomName == 'W16S67') {
                newClaimRoom = 'W15S67';
                newClaimFlag = 'Claim4';
            } else if (roomName == 'W15S73') {
                newClaimRoom = 'W15S72';
                newClaimFlag = 'Claim5';
            }
            creep.memory.claimRoom = newClaimRoom;
            creep.memory.claimFlag = newClaimFlag;
        }
        
        if (creep.memory.claimRoom == undefined || creep.memory.claimFlag == undefined) {
            setClaim();
        }
        
        if (creep.memory.claiming == undefined) {
            creep.memory.claiming = true;
        }

        if(creep.memory.claiming && creep.hits < creep.hitsMax) {
            creep.memory.claiming = false;
            creep.say('RUN AWAY!');
        }
        if(!creep.memory.claiming && Game.flags['Claim1'] && creep.hits == creep.hitsMax) {
            creep.memory.claiming = true;
            creep.say('GIMME');
        }

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 75);

        if(creep.memory.claiming) {
        	if (creep.room.name == creep.memory.claimRoom) {
        	    myFunctions.doReserve(creep);
        	} else {
        		creep.moveTo(Game.flags[creep.memory.claimFlag]);
        	}
        } else {

            try {
                creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
            } catch (err) {
                Game.notify('Error in new claimer go to spawn code: ' + err.message);
            }

        }
    }
};

module.exports = roleClaimer;