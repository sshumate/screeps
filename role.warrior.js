var rooms = require('rooms');
var myFunctions = require('functions');

var roleWarrior = {

/** @param {Creep} creep **/
    run: function(creep) {

        // ------------------------------------ BEGIN VARIABLES

        const flagPath = [Game.flags.Fight1, Game.flags.Fight2, Game.flags.Fight3, Game.flags.Fight4];
        var waypoint = creep.memory.waypoint;
        
        if (Game.flags.Siege) { const siegeFlag = Game.flags.Siege; }
        if (Game.flags.RedAlert) { const redAlertFlag = Game.flags.RedAlert; }

        //var target;
        var whitelist = rooms.getGlobalWhitelist();
        var enemyCreep = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
            filter: (person) => {
                return whitelist.indexOf(person.owner.username) < 0;
            }
        });
        var rampsAndTowers = creep.room.find(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER ||
                            structure.structureType == STRUCTURE_RAMPART);
                }
            });
        var everythingElse = creep.room.find(FIND_HOSTILE_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType != STRUCTURE_ROAD);
                }
            });

        // ------------------------------------ END VARIABLES
        // ------------------------------------ BEGIN FUNCTIONS

        function attack(target) {
            if(creep.attack(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
                creep.rangedAttack(target);
            }
        }

        function moveAlongPath() {
            
            if (enemyCreep) {
                // if there is an enemy creep, attack it
                attack(enemyCreep);
            } else {
                // check to see if we are at the current waypoint, if we are, set the next waypoint
                // if not, continue moving to the current flag
                if (creep.pos.x == flagPath[waypoint].pos.x && creep.pos.y == flagPath[creep.memory.waypoint].pos.y) {
                    waypoint += 1;
                    creep.memory.waypoint = waypoint % flagPath.length;
                } else {
                    creep.moveTo(flagPath[creep.memory.waypoint]);
                }
            }
        }

        function defendRoom() {

            // this should be for defending their spawn room when no flags are placed
            if (creep.room.name != creep.memory.defendRoom) {
                if (creep.hits < creep.hitsMax) {
                    creep.heal(creep);
                }
                var defendFlag = 'room' + creep.memory.defendRoom;
                creep.moveTo(Game.flags[defendFlag]);
            } else if (enemyCreep) {
                attack(enemyCreep);
            } else {
                if (creep.hits < creep.hitsMax) {
                    creep.heal(creep);
                }
                var defendFlag = 'room' + creep.memory.defendRoom;
                creep.moveTo(Game.flags[defendFlag]);
            }

        }

        function checkRoomsForEnemies() {

            // check all of my rooms for enemies, and if there are enemies, return true
            return false;

        }

        function besiegeRoom() {
            // function to besiege an enemy's room
        }
        
        function setDefendStatus() {
            var defendStatus = false;
            var defendRoomID = 'NONE';
            var roomName = creep.memory.room;
            if (roomName == 'W16S67') {
                defendStatus = true;
                defendRoomID = 'W15S67';
            } else if (roomName == 'W12S64') {
                defendStatus = true;
                defendRoomID = 'W12S65';
            } else if (roomName == 'W15S73') {
                defendStatus = true;
                defendRoomID = 'W15S72';
            }
            
            creep.memory.defend = defendStatus;
            creep.memory.defendRoom = defendRoomID;
        }

        // ------------------------------------ END FUNCTIONS
        // ------------------------------------ BEGIN PROGRAM LOGIC
        
        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 25);
        
        if (creep.memory.defend == undefined || (creep.memory.defend != undefined && creep.memory.defendRoom == undefined)) {
            setDefendStatus();
        }
        
        if (creep.memory.waypoint == undefined) {
            creep.memory.waypoint = 0;
        }
        
        /*
        try {
            if (creep.hits < creep.hitsMax) {
                creep.heal(creep);
            }
        } catch (err) {
            console.log(err.message);
        }
        */
        
        if (Memory.redAlert == true && redAlertFlag) {
            if (redAlertFlag.room.name && redAlertFlag.room.name == creep.room.name) {
                attack(enemyCreep);
            } else {
                creep.moveTo(redAlertFlag);
            }
        } else if (enemyCreep) {
            attack(enemyCreep);
        } else if (creep.memory.defend == true) {
            defendRoom();    
        } else if (Game.flags.Siege) {
            // siege function goes here
            // besiegeRoom();
        } else {
            moveAlongPath();
        }
        // ------------------------------------ END PROGRAM LOGIC
    }
};

module.exports = roleWarrior;