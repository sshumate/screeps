var rooms = require('rooms');
var myFunctions = require('functions');

var roleUpgrader = {

    /** @param {Creep} creep **/
    run: function(creep) {
        
        // ------------------------------------ BEGIN FUNCTIONS
        
        function retrieveFromLink() {
            var receivers = rooms.getReceivers(creep.room.name);
            if (receivers.length > 0) {
                var link = Game.getObjectById(receivers[0]); // fix this later
            } else {
                return false;
            }
            
            if ( link.energy > ( creep.carryCapacity / 2 ) ) {
                if (creep.withdraw(link, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(link);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function retrieveFromStorage() {
            var storages = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                    }
                });
            if (storages.length) {
                if(creep.withdraw(storages[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(storages[0]);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function retrieveFromContainers() {
            var containers = creep.room.find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) > creep.carryCapacity;
                    }
                });
            if (containers.length) {
                var container = creep.pos.findClosestByPath(containers);
                if(creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(container);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function retrieveFromSpawn() {
            // for new rooms - pull energy to upgrade from spawn because no containers exist
            // maybe remove this later
            var spawn = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_SPAWN) && structure.energy > creep.carryCapacity;
                }
            });
            if (spawn.length > 0) {
                if (creep.withdraw(spawn[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                    creep.moveTo(spawn[0]);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function upgradeController() {
            if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(creep.room.controller);
            }
        }

        // ------------------------------------ END FUNCTIONS
    	// ------------------------------------ BEGIN UPGRADING STATUS
        
        if(creep.memory.upgrading && creep.carry.energy == 0) {
            creep.memory.upgrading = false;
            creep.say('get da nrg');
        }
        if(!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
            creep.memory.upgrading = true;
            creep.say('upgrading');
        }
        
        // ------------------------------------ END DELIVERY STATUS
    	// ------------------------------------ BEGIN PROGRAM LOGIC

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);

        if (creep.room.name != creep.memory.room) {
	    	creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
	    } else if(creep.memory.upgrading) {
            upgradeController();
        } else {
            
            if ( retrieveFromLink() ) {
            } else if ( retrieveFromStorage() ) {
            } else if ( retrieveFromContainers() ) {
            } else if ( retrieveFromSpawn() ) {
            } else {
                console.log("Upgrader has no energy to retrieve.");   
            }
        }
    }
};

module.exports = roleUpgrader;