var rooms = require('rooms');
var myFunctions = require('functions');

var roleLinker = {
    
    run: function(creep) {

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);
        
        creep.memory.x = 27;
        creep.memory.y = 27; // CHANGE LATER !!!!!!!!
        if (creep.pos.x != creep.memory.x && creep.pos.y != creep.memory.y) {
            // first, if we're not in place, get there
            creep.moveTo(creep.memory.x, creep.memory.y);
            creep.say("Gooooing");
            
        } else {
            
            if (creep.carry.energy < creep.carryCapacity) {
                
                //creep.say("Getting!");
                // if we don't have enough energy, fill up from the storage
                var storages = creep.room.find(FIND_STRUCTURES, {
                  filter: (structure) => {
                      return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                  }
                });
                if (storages.length > 0) {
        
                    // storage has energy for us, withdraw it
                    var target = creep.pos.findClosestByRange(storages);
                    
                    creep.moveTo(target);
                    creep.withdraw(target, RESOURCE_ENERGY);
                    //if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    //    creep.moveTo(target);
                    //}
        
                } else {
                    console.log("no energy, crap");
                }
            } else {
                
                //creep.say("Transferring");
                // get the sender link and, if it's not full, transfer energy to it
                var sender = Game.getObjectById(rooms.getSenders(creep.room.name));
                
                if ( sender.energy < sender.energyCapacity ) {
                    creep.transfer(sender, RESOURCE_ENERGY);
                }
            }
            
        }
    }
    
    
    
    
};

module.exports = roleLinker;