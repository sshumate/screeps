var rooms = require('rooms');
var myFunctions = require('functions');

var roleRepairer = {

    /** @param {Creep} creep **/
    run: function (creep) {

        var otherRooms = new Array();
        for (var room in Game.rooms) {
          if (myFunctions.getRoomDistance(creep.room.name, Game.rooms[room].name) == 1) {
            otherRooms.push(Game.rooms[room].name);
          }
        }
        const BUILDING_PCT = .66;
        const VERBOSE = false;
        
        myFunctions.monitorLife(creep, 0);

        function getEnergy() {
          // look for a storage to get energy from and then retrieve it
          if (creep.room.name != creep.memory.room) {
              creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
          } else {
            var storages = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                }
            });
            if(storages.length > 0) {

                var target = creep.pos.findClosestByPath(storages);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }

            } else {

              // if no storage has energy, search for a container
              var containers = creep.room.find(FIND_STRUCTURES, {
                  filter: (structure) => {
                      return (structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) > creep.carryCapacity;
                  }
              });
              if(containers.length > 0) {
                var target = creep.pos.findClosestByPath(containers);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
              } else {
                  // if no storages and no containers, search main room
                  var mainStorages = Game.rooms[creep.memory.room].find(FIND_STRUCTURES, {
                     filter: (structure) => {
                         return (structure.structureType == STRUCTURE_STORAGE) && _.sum(structure.store) > creep.carryCapacity;
                     } 
                  });
                  //console.log(mainStorages);
                  if (mainStorages.length > 0) {
                      creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
                  } else {
                      // NO LUCK ANYWHERE PULL FROM THE SPAWN
                    var spawn = creep.room.find(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return (structure.structureType == STRUCTURE_SPAWN) && structure.energy > creep.carryCapacity;
                        }
                    });
                    if (spawn.length > 0) {
                        if (creep.withdraw(spawn[0], RESOURCE_ENERGY) === ERR_NOT_IN_RANGE) {
                            creep.moveTo(spawn[0]);
                        }
                        return true;
                    }
                }
              }
            }
          }
        }

        function getNewRepair() {

          var needRepair = Game.rooms[creep.memory.room].find(FIND_STRUCTURES, {
                 filter: function(object){
                    return (object.structureType === STRUCTURE_ROAD ||
                            object.structureType === STRUCTURE_CONTAINER) && (object.hits < (BUILDING_PCT * object.hitsMax));
                 }
          });
          
          if (needRepair.length > 0) {

            // we found a road, set it to the repair ID
            if (creep.room.name != creep.memory.room) {
              var roadToRepair = needRepair[0];
            } else {  
              var roadToRepair = creep.pos.findClosestByPath(needRepair);
            }
            
            try {
              creep.memory.repairID = roadToRepair.id;
              creep.memory.destination = roadToRepair.room.name;
              if (VERBOSE) { console.log(creep.name + " assigning " + roadToRepair.structureType + " to repair with ID: " + roadToRepair.id + " in room " + roadToRepair.room.name); }
            } catch (err) {
              console.log("Error in Repair (" + creep.name + "): " + err.message);
              creep.memory.repairID = 0;
            }

          } else {

            
            var needRepair = Game.rooms[creep.memory.room].find(FIND_STRUCTURES, {
                   filter: function(object){
                      return (object.structureType === STRUCTURE_WALL ||
                              object.structureType === STRUCTURE_RAMPART) && (object.hits < (rooms.getRoom(creep.memory.room).WALL_HP));
                   }
            });
            var wallToRepair = creep.pos.findClosestByPath(needRepair);

            if (wallToRepair) {

              creep.memory.repairID = wallToRepair.id;
              creep.memory.destination = wallToRepair.room.name;
              if (VERBOSE) { console.log(creep.name + " assigning " + wallToRepair.structureType + " to repair with ID: " + wallToRepair.id + " in room " + wallToRepair.room.name); }

            } else {

              // search other rooms and repair them
              //console.log(creep.name + ' looking for repair in other room');
              for (var j in otherRooms) {
                try {
                  var needRepair = Game.rooms[otherRooms[j]].find(FIND_STRUCTURES, {
                         filter: function(object){
                            return (object.structureType === STRUCTURE_ROAD ||
                                    object.structureType === STRUCTURE_CONTAINER) && (object.hits < (BUILDING_PCT * object.hitsMax));
                         }
                  });
                } catch (err) {
                  console.log(err.message);
                }
                  
                //console.log(needRepair.length);
                if (needRepair.length > 0) {

                  // we found a road, set it to the repair ID
                  if (creep.room.name != needRepair[0].room.name) {
                    var roadToRepair = needRepair[0];
                  } else {  
                    var roadToRepair = creep.pos.findClosestByPath(needRepair);
                  }
                  try {
                    creep.memory.repairID = roadToRepair.id;
                    creep.memory.destination = roadToRepair.room.name;
                    console.log(creep.name + " assigning " + roadToRepair.structureType + " to repair with ID: " + roadToRepair.id + " in other room " + roadToRepair.room.name);
                  } catch (err) {
                    console.log("Error in Repair (" + creep.name + "): " + err.message);
                    creep.memory.repairID = 0;
                  }
                  break;

                }
              }
            }
          }
        }

        function repairFromMemory() {

          // we have a thing we are repairing in memory, get that object by the stored ID
          // then move to and repair that item.
          if (creep.room.name != creep.memory.destination) {
            creep.moveTo(Game.flags['room' + creep.memory.destination]);
          } else {
            var thingToRepair = Game.getObjectById(creep.memory.repairID);


            if (thingToRepair && (thingToRepair.structureType === STRUCTURE_WALL || thingToRepair.structureType === STRUCTURE_RAMPART)) {

              if ( thingToRepair.structureType == STRUCTURE_WALL && thingToRepair.hits < (rooms.getRoom(creep.memory.room).WALL_HP) ) {
                
                // if it is a wall, repair it to its assigned HP
                var repairResult = creep.repair(thingToRepair);
                if (repairResult == ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                myFunctions.checkRepair(repairResult);
              } else if ( thingToRepair.structureType == STRUCTURE_RAMPART && thingToRepair.hits < ( rooms.getRoom(creep.memory.room).WALL_HP + (rooms.getRoom(creep.memory.room).WALL_HP * .25) ) ) {
                // if it is a rampart, repair it to 25% more than its assigned HP
                var repairResult = creep.repair(thingToRepair);
                if (repairResult == ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                myFunctions.checkRepair(repairResult);
              } else {
                creep.memory.repairID = 0;
              }

            } else {

              // if it is anything other than a wall, repair it to full
              if (thingToRepair && thingToRepair.hits < thingToRepair.hitsMax) {

                var repairResult = creep.repair(thingToRepair);
                if (repairResult == ERR_NOT_IN_RANGE) {
                  creep.moveTo(thingToRepair);
                }
                myFunctions.checkRepair(repairResult);
                
              } else {
                creep.memory.repairID = 0;
              }
            }
          }
        }
        
        if (creep.memory.repairID == undefined) {
            creep.memory.repairID = 0;
        }

        if(creep.carry.energy === 0) {

          getEnergy();

        } else if (creep.memory.repairID === 0) {

          // if we have energy and there is no item we are currently repairing, find a new one
          getNewRepair();

        } else {

          // we already have something in memory, let's repair it
          repairFromMemory();

        }
    }
};

module.exports = roleRepairer;