var rooms = require('rooms');
var myFunctions = require('functions');

var roleKiter = {

/** @param {Creep} creep **/
    run: function(creep) {
        
    if (creep.memory.room == 'W14S68') {
        var flag1 = Game.flags.build1;
        var flag2 = Game.flags.kite1;
        var tickNum = 100;
    }
     
    myFunctions.monitorLife(creep, 0);
    
    if (creep.memory.moveAway == true) {
        if (creep.memory.wp1 == false) {
            creep.moveTo(flag1);
            if (creep.pos.getRangeTo(flag1) < 4) {
                creep.memory.wp1 = true;
                creep.memory.wp2 = false;
            }
        } else if (creep.memory.wp2 == false) {
            creep.moveTo(flag2);
            if (creep.pos.getRangeTo(flag2) < 4) {
                creep.memory.wp2 = true;
                creep.memory.wp1 = false;
            }
        }
    }
    
    if (!creep.memory.moveAway) {
        creep.memory.moveAway = true;
    }
        
    
    if (creep.memory.healUp == true) {          
        creep.heal(creep);      
        if (creep.hits > (.99 * creep.hitsMax)) {        
            creep.memory.healUp = false;
        }
    }
        
    var target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
        filter: (hostile) => {
            return (hostile.owner.username != 'Zoneman');
        }
    });
        
              	
    if (target) {
        if (creep.pos.getRangeTo(target) > 3) {      
            creep.memory.moveAway = false;        
            creep.moveTo(target);
        } else if (creep.pos.getRangeTo(target) < 4) {         
            creep.rangedAttack(target);          
            creep.memory.moveAway = true;
        }
                
        if (creep.hits < creep.hitsMax) {      
            creep.memory.healUp = true;      
            creep.memory.moveAway = true;
        }
    } else if (!target) {  
        var ytarget = creep.pos.findClosestByRange(FIND_MY_CREEPS, {
            filter: function(object) {return object.hits < object.hitsMax;
            }
        });
                
        var ztarget = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
            filter: (hostile) => {
                return (hostile.owner.username == 'Zoneman') && hostile.hits < hostile.hitsMax;
                }
            });
            
        if(ytarget || ztarget) {
            creep.moveTo(ytarget || ztarget);
            if(creep.pos.isNearTo(ytarget || ztarget)) {
                creep.heal(ytarget || ztarget);
            } else {
                creep.rangedHeal(ytarget || ztarget);
                }
            }
        }
    }
};

module.exports = roleKiter;