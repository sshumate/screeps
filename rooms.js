/* rooms.js
 * This file contains a variable, myRooms, which contains information about each room under my control.
 * It also has a variable containing getter functions that we export as a module so that other roles can make
 * use of the room data. This allows for a centralized location for room data, and simplifies the role's logic.
 
 * To include:
 * var rooms = require('rooms.js');
 * 
 * Potential uses:
 * --
 * var myRooms = rooms.getRooms();          // returns an object with all rooms and their data
 * var myRoomName = 'W12S63';
 * console.log(myRooms[myRoomName].spawnName); // returns the name of the spawn in W12S63
 * --
 * var myRoom = rooms.myRoomName;              // returns the object of one room and all of its data
 * --
 * var spawnName = rooms.getRoom('W12S63').spawnName; // returns spawn name, in-line usage of getRoom function
 */

var myRooms = {
        'W8N3' : {
            // main room
            roomName :          'W8N3',
            spawnName :         'Zone1',
            NUM_HARVESTERS :    2,
            NUM_TRUCKERS :      2,
            NUM_UPGRADERS :     2,
            NUM_BUILDERS :      1,
            NUM_REPAIRERS :     1,
            NUM_EXP_HARVESTERS: 0,
            NUM_CLAIMERS :      0,
            NUM_WARRIORS :      0,
            NUM_HEALERS :       0,
            NUM_LINKERS :       1,
            NUM_GRABBERS :      0,
            NUM_MINERS :        0,
            NUM_SKIRMISHERS :   0,
            WALL_HP :           100000,
            RAMPART_HP :        100000,
            IDLE_X :            30,
            IDLE_Y :            26,
            pickupContainers :  [
                "5df9e43447ebdc383206b0ee",
                "5df9ee5935949e38331a8396"
            ],
            dropoffContainers : [
                ""
            ],
            senders : [
                "5dfc038358c3b0363f3b30b4"
            ],
            receivers : [
                "5dfc06146986bf36426aee4b"
            ],
            playerWhitelist : []
        }
    };
    
var globalWhiteList = [
        'Xaq',
        'Irontomcash'
    ];

var myRoomsFunc = {
	getRoom: function(roomName) {
        return myRooms[roomName];
	},

	getRooms: function() {
		return myRooms;
	},
	
	getPickupsByRoom: function(roomName) {
	    return myRooms[roomName].pickupContainers;
	},
	
	getDropoffsByRoom: function(roomName) {
	    return myRooms[roomName].dropoffContainers;
	},

    getRoomWhitelist: function(roomName) {
        return myRooms[roomName].playerWhitelist;
    },

    getGlobalWhitelist: function() {
        return globalWhiteList;
    },
    getIdleLocation: function(roomName) {
        location = new Array();
        location['x'] = myRooms[roomName].IDLE_X;
        location['y'] = myRooms[roomName].IDLE_Y;
        return location;
    },
    getSenders: function(roomName) {
        return myRooms[roomName].senders;
    },
    getReceivers: function(roomName) {
        return myRooms[roomName].receivers;
    }
};

module.exports = myRoomsFunc;