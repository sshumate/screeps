var rooms = require('rooms');
var myFunctions = require('functions');

var roleMiner = {

    run: function(creep) {

        // ------------------------------------ BEGIN FUNCTIONS

        function deliverToTerminal() {
            
            // check for towers and containers who need energy
            // if one exists, transfer/move to it, return true
            // if none, return false
            var terminals = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_TERMINAL && _.sum(structure.store) < structure.storeCapacity;
                }
            });
            if(terminals.length > 0) {
                var target = creep.pos.findClosestByPath(terminals, 10);
                for (var type in RESOURCES_ALL) {
                    if(creep.transfer(target, RESOURCES_ALL[type]) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target);
                    }  else {
                        //console.log('miner transfer error: ' + transferResult);
                    }
                }
                return true;
            } else {
                return false;
            }
        }

        function deliverToContainers() {
            
            // check for towers and containers who need energy
            // if one exists, transfer/move to it, return true
            // if none, return false
            var containers = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_CONTAINER && _.sum(structure.store) < structure.storeCapacity;
                }
            });
            if(containers.length > 0) {
                var target = creep.pos.findClosestByPath(containers, 10);
                for (var type in RESOURCES_ALL) {
                    if(creep.transfer(target, type) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target);
                    }
                }
                return true;
            } else {
                return false;
            }
        }
        
        function goMining() {
            // go mine at this creep's mine source
            var sources = creep.room.find(FIND_MINERALS);
            var extractor = creep.room.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_EXTRACTOR}});
            if (extractor.length > 0 && extractor[0].cooldown == 0) {
                var harvestResult = creep.harvest(sources[0]);
                if(harvestResult == ERR_NOT_IN_RANGE || harvestResult == ERR_NOT_ENOUGH_RESOURCES) {
                    creep.moveTo(sources[0]);
                } else {
                    //console.log("Harvest result error: " + harvestResult);
                }
            }
        }

        // ------------------------------------ END FUNCTIONS
        // ------------------------------------ BEGIN MINING STATUS
        
        if (creep.memory.mining == undefined) {
            creep.memory.mining = true;
        }

	   if ( !creep.memory.mining && _.sum(creep.carry) === 0 ) {
            // if the creep is carrying less than max energy, go mine
            creep.memory.mining = true;
        } else if ( creep.memory.mining && _.sum(creep.carry) == creep.carryCapacity ) {
            // if the creep is mining and at full energy, go drop off at a container
            creep.memory.mining = false;
        }

        // ------------------------------------ END MINING STATUS
        // ------------------------------------ BEGIN PROGRAM LOGIC

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);

        if (creep.memory.mining === true) {

            if (creep.room.name != creep.memory.room) {
                creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
            } else {
                // get mine source from creep's memory, search for sources, and attempt to mine that source
                goMining();
            }

        } else if (creep.memory.mining === false) {

            if ( deliverToTerminal() ) {
            } else if ( deliverToContainers() ) {
            } else {
                console.log('miner is fucked in ' + creep.room.name);
            }

        }
    }
	
};

module.exports = roleMiner;