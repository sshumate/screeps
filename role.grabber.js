var rooms = require('rooms');
var myFunctions = require('functions');

var roleGrabber = {

    /** @param {Creep} creep **/
    run: function(creep) {

        const VERBOSE = false;
        const claimRoom = 'W17S74';
        const returnTime = 150;

        myFunctions.monitorLife(creep, returnTime);
        
        function upgradeController() {
            if (creep.carry.energy > 50) {
                if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(creep.room.controller);
                }
            } else {
                mine();
            }
        }
        
        function mine() {
            var mineSource = creep.memory.mineSource;
            //var sources = creep.room.find(FIND_SOURCES);
            //var harvestResult = creep.harvest(sources[mineSource]);
            var source = creep.pos.findClosestByPath(FIND_SOURCES);
            var harvestResult = creep.harvest(source);
            if(harvestResult == ERR_NOT_IN_RANGE || harvestResult == ERR_NOT_ENOUGH_RESOURCES) {
                //creep.moveTo(sources[mineSource]);
                creep.moveTo(source);
            }
        }
        
        function build() {
            if(creep.memory.building && creep.carry.energy == 0) {
                // if the creep is building and runs out of energy, go get energy
                creep.memory.building = false;
                creep.say('Work work.');
            }
            if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
                // if the creep is getting energy and has topped off, go build
                creep.memory.building = true;
                creep.say('Hammer time.');
            }

            if (creep.memory.building) {

                var currentRoomTargets = creep.room.find(FIND_CONSTRUCTION_SITES);
                if(currentRoomTargets.length) {

                    //var target = creep.pos.findClosestByPath(currentRoomTargets);
                    var target = creep.pos.findClosestByRange(currentRoomTargets);
                    if(creep.build(target) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(target);
                    }

                } else {
                    upgradeController();
                }
            } else if (!creep.memory.building) {
                mine();
            }
        }
        
        function setMineSource() {
            var roomName = creep.memory.room;
            var grabbers = _.filter(Game.creeps, (guy) => { return guy.memory.role == 'grabber' && guy.memory.room == roomName && guy.memory.fading == false });
            var newMineSource;
            if (grabbers.length > 0) {
                if (grabbers[0].memory.mineSource == 0) {
                    newMineSource = 1;
                } else {
                    newMineSource = 0;
                }
            } else {
                newMineSource = 0;
            }
            creep.memory.mineSource = newMineSource;
        }
        
        if (creep.memory.WP1 == undefined) {
            creep.memory.WP1 = false;
            creep.memory.WP2 = false;
            creep.memory.WP3 = false;
            creep.memory.WP4 = false;
            creep.memory.WP5 = false;
            creep.memory.WP6 = false;
        }
        
        if (creep.memory.building == undefined) {
            creep.memory.building = true;
        }

        if (creep.memory.WP1 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 1, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP1);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP1) < 1 ) {
                creep.memory.WP1 = true;
                if (VERBOSE == true ) { console.log('wp1'); }
            }
        } else if (creep.memory.WP2 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 2, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP2);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP2) < 1 ) {
                creep.memory.WP2 = true;
                if (VERBOSE == true ) { console.log('wp2'); }
            }
        } else if (creep.memory.WP3 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 3, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP3);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP3) < 1 ) {
                creep.memory.WP3 = true;
                if (VERBOSE == true ) { console.log('wp3'); }
            }
        } else if (creep.memory.WP4 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 4, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP4);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP4) < 1 ) {
                creep.memory.WP4 = true;
                if (VERBOSE == true ) { console.log('wp4'); }
            }
        } else if (creep.memory.WP5 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 5, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP5);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP5) < 1 ) {
                creep.memory.WP5 = true;
                if (VERBOSE == true ) { console.log('wp5'); }
            }
        } else if (creep.memory.WP6 == false) {
            if (VERBOSE == true ) { console.log(creep.name + ' going to flag 6, currently in ' + creep.room.name + ' (' + creep.ticksToLive + ')'); }
            creep.moveTo(Game.flags.GrabWP6);
            if ( creep.pos.getRangeTo(Game.flags.GrabWP6) < 1 ) {
                creep.memory.WP6 = true;
                if (VERBOSE == true ) { console.log('wp6'); }
            }
        } else {
            
            if (creep.room.name == claimRoom && Game.rooms[creep.room.name].controller.owner && Game.rooms[creep.room.name].controller.owner.username == 'Zoneman') {
                
                if (Game.rooms[creep.room.name].controller.ticksToDowngrade < 3000) {
                    upgradeController();
                } else {
                    build();
                }
                
            } else if (creep.room.name == claimRoom) {
                myFunctions.doClaim(creep);
            } else {
                console.log('issue in grabber logic');
            }
            
        }

        
        if (creep.hits < creep.hitsMax) {
            try {
                creep.heal(creep);
            } catch (err) {
                console.log(err.message);
            }
        }
        
    }
};

module.exports = roleGrabber;