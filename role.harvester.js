var rooms = require('rooms');
var myFunctions = require('functions');

var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {

        // ------------------------------------ BEGIN FUNCTIONS
        
        function deliverToSpawn() {
            
            // check for spawn and extension targets
            // find the closest and transfer or move to it, return true
            // if no targets, return false
            var targets = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION ||
                            structure.structureType == STRUCTURE_SPAWN) && structure.energy < structure.energyCapacity;
                }
            });

            if(targets.length > 0) {
                var target = creep.pos.findClosestByPath(targets);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                    
                }
                return true;
            } else {
                return false;
            }
        }
        
        function deliverToContainers() {
            
            // check for towers and containers who need energy
            // if one exists, transfer/move to it, return true
            // if none, return false
            var containers = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER ||
                            structure.structureType == STRUCTURE_CONTAINER) && _.sum(structure.store) < structure.storeCapacity;
                }
            });
            if(containers.length > 0) {
                var target = creep.pos.findClosestByPath(containers, 10);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function deliverToLinks() {
            var links = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return structure.structureType == STRUCTURE_LINK &&
                           structure.energy < structure.energyCapacity &&
                           creep.pos.getRangeTo(structure) <= 3;
                }
            });
            if(links.length > 0) {
                var target = creep.pos.findClosestByPath(links);
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
                return true;
            } else {
                return false;
            }
        }
        
        function goMining() {
            // go mine at this creep's mine source
            var mineSource = creep.memory.mineSource;
            var sources = creep.room.find(FIND_SOURCES);
            var harvestResult = creep.harvest(sources[mineSource]);
            if(harvestResult == ERR_NOT_IN_RANGE || harvestResult == ERR_NOT_ENOUGH_RESOURCES) {
                creep.moveTo(sources[mineSource]);
            } else {
                //console.log("Harvest result error: " + harvestResult);
            }
        }
        
        function checkTruckers() {
            // find all the truckers currently in the room and return that number
            var truckers = _.filter(Game.creeps, (trucker) => { return trucker.memory.role == 'trucker' && trucker.memory.room == creep.memory.room });
            return truckers.length;
        }
        
        function setMineSource() {
            var harvesters = _.filter(Game.creeps, (guy) => { return guy.memory.role == 'harvester' && guy.memory.room == creep.memory.room && guy.memory.fading == false });
            var newMineSource = 0;
            var roomName = creep.memory.room;
            if (roomName == 'W12S64') {
                newMineSource = 1;
            } else if (roomName == 'W11S65') {
                var source_zero = 0;
                for (var i = 0; i < harvesters.length; i++) {
                    if (harvesters[i].memory.mineSource == 0) {
                        source_zero++;
                    }
                }

                if (source_zero > 0) {
                    newMineSource = 1;
                } else {
                    newMineSource = 0;
                }
            } else if (roomName == 'W13S65') {
                var source_zero = 0;
                for (var i = 0; i < harvesters.length; i++) {
                    if (harvesters[i].memory.mineSource == 0) {
                        source_zero++;
                    }
                }

                if (source_zero >= 1) {
                    newMineSource = 1;
                } else {
                    newMineSource = 0;
                }
            } else if (roomName == 'W15S73') {
                var source_zero = 0;
                for (var i = 0; i < harvesters.length; i++) {
                    if (harvesters[i].memory.mineSource == 0) {
                        source_zero++;
                    }
                }

                if (source_zero > 0) {
                    newMineSource = 1;
                } else {
                    newMineSource = 0;
                }
            } else if (roomName == 'W17S74') {
                var source_zero = 0;
                for (var i = 0; i < harvesters.length; i++) {
                    if (harvesters[i].memory.mineSource == 0) {
                        source_zero++;
                    }
                }

                if (source_zero > 0) {
                    newMineSource = 1;
                } else {
                    newMineSource = 0;
                }
            } else if (harvesters.length > 0 && harvesters[0].memory.mineSource == 0) {
                newMineSource = 1;
            } else {
                newMineSource = 0;
            }
            
            creep.memory.mineSource = newMineSource;
        }

        // ------------------------------------ END FUNCTIONS
        // ------------------------------------ BEGIN MEMORY MANAGEMENT

        if (creep.memory.mineSource == undefined) {
            setMineSource();
        }
        
        if (creep.memory.mining == undefined) {
            creep.memory.mining = true;
        }

        if(creep.carry.energy === 0 && creep.memory.mining == false) {
            // if the creep is carrying less than max energy, go mine
            creep.memory.mining = true;
        } else if (creep.memory.mining == true && creep.carry.energy == creep.carryCapacity) {
            // if the creep is mining and at full energy, go drop off at a container
            creep.memory.mining = false;
        }

        // ------------------------------------ END MEMORY MANAGEMENT
        // ------------------------------------ BEGIN PROGRAM LOGIC

        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 0);

        if (creep.memory.mining === true) {

            if (creep.room.name != creep.memory.room) {
                creep.moveTo(Game.spawns[rooms.getRoom(creep.memory.room).spawnName]);
            } else {
                // get mine source from creep's memory, search for sources, and attempt to mine that source
                goMining();
            }

        } else if (creep.memory.mining === false) {

            // Make sure we have truckers. If none, deliver to spawn yourself
            // Otherwise, Search for containers with enough storage space
            // if there is a valid container, move to it and transfer energy
            // to do: make sure container is within a certain range
            if ( checkTruckers() == 0 ) {
                deliverToSpawn();
            } else if ( deliverToLinks() ) {
            } else if ( deliverToContainers() ) {
                
            } else {
                // deliver energy to spawn
    	    	deliverToSpawn();
            }

        }
    }
};

module.exports = roleHarvester;