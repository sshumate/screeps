/*
 * This functions file contains functions common to multiple roles and/or files.
 * Each function should have a description of its purpose, but be general enough
 * that others can make use of it. A function specific to a role should not go into this file.
 *
 * To use this file, require it:
 * var myFunctions = require('functions');
 *
 * To use a function of this file after it has been required:
 * myFunctions.function(arguments);
 *
 */

// get the current set of all our types of creeps

var rooms = require('rooms');

module.exports = {
    
    // ------------------ COMMAND LINE HELPER FUNCTIONS
    termSend: function(source, destination, resource, amt) {
        Game.rooms[source].terminal.send(resource, amt, destination);
        return "";
    },
    
    sellResource: function(resource, room, amt, cost) {
        // this function sells a resource (resource) from room (room) in the amount (amt) specified
        // it searches for all buy orders for that resource with a transaction cost of less than energy specified (cost)
        allOrders = Game.market.getAllOrders(
            order => order.resourceType == resource &&
                     order.type == ORDER_BUY &&
                     Game.market.calcTransactionCost(amt, room, order.roomName) < cost
            );
        allOrders.sort(function(a, b){return b.price-a.price});
        if (allOrders.length > 0) {
            var result = Game.market.deal(allOrders[0].id, amt, room);
            this.checkMarketTransaction(result);
            return result;
        } else {
            return "No orders";
        }
    },
    
    testSellResource: function(resource, room, amt) {
        // this function does a "test sell" and reports on available orders to the console
        // no selling actually takes place
        allOrders = Game.market.getAllOrders(
            order => order.resourceType == resource &&
                     order.type == ORDER_BUY
            );
        allOrders.sort(function(a, b){return b.price-a.price});
        if (allOrders.length > 0) {
            for (var id in allOrders) {
                console.log("-----" + id);
                console.log("Room: " + allOrders[id].roomName);
                console.log("Amount: " + allOrders[id].remainingAmount + " / " + allOrders[id].amount);
                console.log("Price: " + allOrders[id].price);
            }
            var transactionCost = Game.market.calcTransactionCost(amt, room, allOrders[0].roomName);
            var result = "Cost to sell " + amt + " of " + resource + " is: " + transactionCost;
            return result;
        } else {
            return "No orders";
        }
    },
    
    addLink: function(linkType, linkID) {
        // linkType should be either "sender" or "receiver"
        // link ID is the object's game ID
        // this function adds information to the game's link memory
        var link = Game.getObjectById(linkID);
        var roomName = link.room.name;
        
        if (!Game.links[roomName]) {
            Game.links[roomName] = new Object();
        }
        
        if (linkType == "sender") {
            if (!Game.links[roomName].senders) {
                Game.links[roomName].senders = new Array();
            }
            Game.links[roomName].senders.push(linkID);
        } else if (linkType == "receiver") {
            if (!Game.links[roomName].receivers) {
                Game.links[roomName].receivers = new Array();
            }
            Game.links[roomName].receivers.push(linkID);
        }
    },
    
    linkCleanup: function(roomName) {
        // roomName is just a room name
        // this function will search each ID stored in memory
        // if the object cannot be obtained, we'll delete it from memory
        // if the room name is "all", we'll loop through all of our rooms
        
        
    },
    
    // ------------------- REPORTING & EMAILS
    
    reportStatus: function() {
        var allHarvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
        var allTruckers = _.filter(Game.creeps, (creep) => creep.memory.role == 'trucker');
        var allUpgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');
        var allBuilders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
        var allRepairers = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairer');
        var allWarriors = _.filter(Game.creeps, (creep) => creep.memory.role == 'warrior');
        var allHealers = _.filter(Game.creeps, (creep) => creep.memory.role == 'healer');
        var allExp_harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'exp_harvester');
        var allClaimers = _.filter(Game.creeps, (creep) => creep.memory.role == 'claimer');
        var allLinkers = _.filter(Game.creeps, (creep) => creep.memory.role == 'linker');
        var allGrabbers = _.filter(Game.creeps, (creep) => creep.memory.role == 'grabber');
        var allMiners = _.filter(Game.creeps, (creep) => creep.memory.role == 'miner');
        var allSkirmishers = _.filter(Game.creeps, (creep) => creep.memory.role == 'skirmisher');
        // report it to the log
        console.log("-----");
        console.log('Harvesters: ' + allHarvesters.length + ' | Truckers: ' + allTruckers.length);
        console.log('Upgraders:  ' + allUpgraders.length + ' | Builders: ' + allBuilders.length);
        console.log('Repairers:  ' + allRepairers.length + ' | Claimers: ' + allClaimers.length);
        console.log('Expedition Harvesters:    ' + allExp_harvesters.length);
        console.log('Warriors:   ' + allWarriors.length + ' | Healers:  ' + allHealers.length);
        console.log('Grabbers:    ' + allGrabbers.length + ' | Miners: ' + allMiners.length);
        console.log('Linkers:    ' + allLinkers.length + ' | Skirmishers: ' + allSkirmishers.length);
        for (var id in Game.structures) {
            if (Game.structures[id].structureType == STRUCTURE_STORAGE) {
                var storage = Game.structures[id];
                var hourEnergyChange = this.manageEnergyGain(storage.room.name, 'HOUR', false);
                console.log(storage.room.name + ' Storage energy: ' + _.sum(storage.store) + ' (' + hourEnergyChange + ')');
            }
        }
        return "";
    },
    
    emailStatus: function() {
        
        var allHarvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'harvester');
        var allTruckers = _.filter(Game.creeps, (creep) => creep.memory.role == 'trucker');
        var allUpgraders = _.filter(Game.creeps, (creep) => creep.memory.role == 'upgrader');
        var allBuilders = _.filter(Game.creeps, (creep) => creep.memory.role == 'builder');
        var allRepairers = _.filter(Game.creeps, (creep) => creep.memory.role == 'repairer');
        var allWarriors = _.filter(Game.creeps, (creep) => creep.memory.role == 'warrior');
        var allHealers = _.filter(Game.creeps, (creep) => creep.memory.role == 'healer');
        var allExp_harvesters = _.filter(Game.creeps, (creep) => creep.memory.role == 'exp_harvester');
        var allClaimers = _.filter(Game.creeps, (creep) => creep.memory.role == 'claimer');
        var allLinkers = _.filter(Game.creeps, (creep) => creep.memory.role == 'linker');
        var allGrabbers = _.filter(Game.creeps, (creep) => creep.memory.role == 'grabber');
        var allMiners = _.filter(Game.creeps, (creep) => creep.memory.role == 'miner');
        var allSkirmishers = _.filter(Game.creeps, (creep) => creep.memory.role == 'skirmisher');
        var body = '';
        body += '-----\n';
        body += 'Harvesters: ' + allHarvesters.length + ' | Truckers: ' + allTruckers.length + '\n';
        body += 'Upgraders:  ' + allUpgraders.length + ' | Builders: ' + allBuilders.length + '\n';
        body += 'Repairers:  ' + allRepairers.length + ' | Claimers: ' + allClaimers.length + '\n';
        body += 'Expedition Harvesters:    ' + allExp_harvesters.length + '\n';
        body += 'Warriors:   ' + allWarriors.length + ' | Healers:  ' + allHealers.length + '\n';
        body += 'Grabbers:    ' + allGrabbers.length + ' | Miners: ' + allMiners.length + '\n';
        body += 'Linkers:   ' + allLinkers.length + ' | Skirmishers: ' + allSkirmishers.length + '\n';
        for (var id in Game.structures) {
            if (Game.structures[id].structureType == STRUCTURE_STORAGE) {
                var storage = Game.structures[id];
                var dayEnergyChange = this.manageEnergyGain(storage.room.name, 'DAY', false);
                body += storage.room.name + ' Storage energy: ' + _.sum(storage.store) + ' (' + dayEnergyChange + ')' + '\n';
            }
        }
        Game.notify(body);
        return "";
    },
    
    manageEnergyGain: function (roomName, period, update) {
        // roomName = the name of the room we are managing energy for
        // period = 1 hour, 24 hour
        // update = true or false, whether or not to update the memory at this time
        if (!Memory.rooms[roomName]) {
            Memory.rooms[roomName] = new Object();
            console.log('Room object for ' + roomName + ' did not exist in manageEnergyGain');
        }
        if (!Memory.rooms[roomName].energy) {
            Memory.rooms[roomName].energy = new Object();
            console.log('Energy object for ' + roomName + ' did not exist in manageEnergyGain');
        }
        
        var d = new Date();
        var currentHour = d.getHours();
        
        for (var id in Game.structures) {
            if (Game.structures[id].structureType == STRUCTURE_STORAGE && Game.structures[id].room.name == roomName) {
                var roomStorage = Game.structures[id];
            }
        }
        
        if (roomStorage) { 
            var currentEnergy = _.sum(roomStorage.store); 
            try {
                var yesterdayEnergy = Memory.rooms[roomName].energy[currentHour];
            } catch (err) {
                var yesterdayEnergy = 0;
            }
            try {
                if (Memory.rooms[roomName].energy[currentHour-1] != undefined) {
                    var lastHourEnergy = Memory.rooms[roomName].energy[currentHour-1];
                } else {
                    var lastHourEnergy = 0;
                }
            } catch (err) {
                var lastHourEnergy = 0;
                console.log('have to set last hour energy to 0');
            }
            
            dayEnergyChange = currentEnergy - yesterdayEnergy;
            hourEnergyChange = currentEnergy - lastHourEnergy;
            
            //console.log(roomName + ' energy gained last hour: ' + hourEnergyChange);
            //console.log(roomName + ' energy gained last 24 hours: ' + dayEnergyChange);
            
            if (update) { Memory.rooms[roomName].energy[currentHour] = currentEnergy; }
            
            if (period == 'DAY') {
                return dayEnergyChange;
            } else if (period == 'HOUR') {
                return hourEnergyChange;
            }
        } else {
            return 0;
        }

    },
    
    reportEnemy: function(creep) {
        var username = creep.owner.username;
        if (username != "Invader") {
            var email = 'User ' + username + ' spotted in my room (' + creep.room.name + ').\n';
            var workCount = 0,
                attackCount = 0,
                rangedAttackCount = 0,
                moveCount = 0,
                healCount = 0,
                toughCount = 0,
                claimCount = 0,
                carryCount = 0;
            
            
            try {
                for (var i = 0; i < creep.body.length; i++) {
                    if (creep.body[i].type == 'move') {
                        moveCount += 1;
                    } else if (creep.body[i].type == 'work') {
                        workCount += 1;
                    } else if (creep.body[i].type == 'carry') {
                        carryCount += 1;
                    } else if (creep.body[i].type == 'attack') {
                        attackCount += 1;
                    } else if (creep.body[i].type == 'ranged_attack') {
                        rangedAttackCount += 1;
                    } else if (creep.body[i].type == 'tough') {
                        toughCount += 1;
                    } else if (creep.body[i].type == 'heal') {
                        healCount += 1;
                    } else if (creep.body[i].type == 'claim') {
                        claimCount += 1;
                    }
                }
                email += '--Body layout--\n';
                email += 'Attack Count: ' + attackCount + '\n';
                email += 'Ranged Attack Count: ' + rangedAttackCount + '\n';
                email += 'Tough Count: ' + toughCount + '\n';
                email += 'Move Count: ' + moveCount + '\n';
                email += 'Heal Count: ' + healCount + '\n';
                email += 'Work Count: ' + workCount + '\n';
                email += 'Carry Count: ' + carryCount + '\n';
                email += 'Claim Count: ' + claimCount + '\n';
            } catch (err) { email += 'could not get body layout'; }
            Game.notify(email);
        }
    },
    
    // ---------------------- CREEP ACTIONS
    
    monitorLife: function(creep, extraTime) {
        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        var ticksLeft = (creep.hitsMax * .03) + extraTime;
        if (creep.memory.fading == false && creep.ticksToLive < ticksLeft) {
            creep.memory.fading = true;
            //console.log(creep.name + ' fading away...');
        }
    },
        
    doReserve: function(creep) {
        if(creep.reserveController(creep.room.controller) == ERR_NOT_IN_RANGE) {
        	creep.moveTo(creep.room.controller);
    	}
    },
    
    doClaim: function(creep) {
        if(creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE) {
        	creep.moveTo(creep.room.controller);
    	}
    },
    
    repairCurrentLocation: function(creep) {
        // this function looks for a structure at the creep's current location, which should be a road
        // if the structure is in need of repair greater than or equal to the creep's capacity to repair, he repairs it, so no energy is wasted
        var workCount = 0;
        var body = creep.body;
        for (var i = 0; i < body.length; i++) {
            if ( body[i].type == 'work') {
                workCount += 1;
            }
        }
        var road = creep.pos.lookFor(LOOK_STRUCTURES);
        var site = creep.pos.lookFor(LOOK_CONSTRUCTION_SITES);
        if ( road.length && (road[0].hitsMax - road[0].hits) >= (workCount * 100) ) {
            var repairResult = creep.repair(road[0]);
            this.checkRepair(repairResult);
        } else if ( site.length ) {
            try {
                var buildResult = creep.build(site[0]);
            } catch (err) {
                console.log("build error: " + err.message);
            }
        }
    },
    
    repairNearbyContainer: function(creep) {
        var workCount = 0;
        var body = creep.body;
        for (var i = 0; i < body.length; i++) {
            if ( body[i].type == 'work' ) {
                workCount += 1;
            }
        }
        var nearbyContainers = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return (structure.structureType == STRUCTURE_CONTAINER) &&
                structure.hits < (.5 * structure.hitsMax) &&
                creep.getRangeTo(structure) <= 3;
            }
        });
        if ( nearbyContainers.length > 0 ) {
            creep.repair(nearbyContainers[0]);
            return true;
        } else {
            return false;
        }
    },
    
    // --------------------- BUILDING ACTIVITIES
    
    runLabs: function(roomName) {
        var labs = new Array();
        
        for (var id in Game.structures) {
            if (Game.structures[id].room.name == roomName && Game.structures[id].structureType == STRUCTURE_LAB) {
                labs.push(Game.structures[id]);
            }
        }
        
        /*
        var spawn = Game.spawns[rooms.getRooms(roomName).spawnName];
        var labs = spawn.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return structure.structureType == STRUCTURE_LAB;
            }
        });
        var terminal = spawn.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                return structure.structureType == STRUCTURE_TERMINAL;
            }
        });
        */
    },
    
    
    
    runLinks: function(roomName) {
        try {
            if (rooms.getSenders(roomName)) {
                var senders = rooms.getSenders(roomName);
            }
            if (rooms.getReceivers(roomName)) {
                var receivers = rooms.getReceivers(roomName);
            }
            /*
            if (Memory.links[roomName].senders) {
                var senders = Memory.links[roomName].senders;
            } else if (rooms.getSenders(roomName)) {
                var senders = rooms.getSenders(roomName);
            }
            if (Memory.links[roomName].receivers) {
                var receivers = Memory.links[roomName].receivers;
            } else if (rooms.getReceivers(roomName)) {
                var receivers = rooms.getReceivers(roomName);
            }*/
        } catch (err) {
            //console.log('Error getting sender and receiver info for room: ' + roomName);
        }
        
        for (var i in receivers) {
            for (var j in senders) {
                var receiver = Game.getObjectById(receivers[i]);
                var sender = Game.getObjectById(senders[j]);
                if ( sender.cooldown == 0 && sender.energy >= (sender.energyCapacity * .5) && receiver.energy <= (receiver.energyCapacity * .6) ) {
                    sender.transferEnergy(receiver);
                }
            }
        }
    },
    
    runTower: function(tower) {
        // tower auto repair and attack method
        // repairs when structures get to 60%, since repairmen should get to them at 66%
        // heals when structures are above 60% and nothing to attack
        if(tower) {
            var whitelist = rooms.getGlobalWhitelist();
            var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
                filter: (person) => {
                    return whitelist.indexOf(person.owner.username) < 0;
                }
            });
            if(closestHostile) {
                tower.attack(closestHostile);
                this.reportEnemy(closestHostile);
            } else {
                var closestDamagedRampart = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType === STRUCTURE_RAMPART) && structure.hits < (.25 * rooms.getRoom(tower.room.name).RAMPART_HP);
                    }
                });
                if (closestDamagedRampart) {
                    tower.repair(closestDamagedRampart);
                }
                var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType != STRUCTURE_WALL &&
                                structure.structureType != STRUCTURE_ROAD &&
                                structure.structureType != STRUCTURE_RAMPART) && structure.hits < (.50 * structure.hitsMax);
                    }
                });
                if(closestDamagedStructure) {
                    tower.repair(closestDamagedStructure);
                } else {
                    var woundedFriendly = tower.pos.findClosestByRange(FIND_MY_CREEPS, {
                        filter: (creep) => {
                            return (creep.hits < creep.hitsMax);
                        }
                    });
                    if(woundedFriendly) {
                        tower.heal(woundedFriendly);
                    }
                }
            }
        }
    },
    
    // --------------------- ERROR CHECKING
    
    
    checkRepair: function(repairResult) {
      if (repairResult === 0) {
        //console.log("Repair result: (" + repairResult + ") The operation has been scheduled successfully.");
      } else if (repairResult === -1) {
        console.log("Repair result: (" + repairResult + ") You are not the owner of this creep.");
      } else if (repairResult === -4) {
        console.log("Repair result: (" + repairResult + ") You are not the owner of this creep.");
      } else if (repairResult === -6) {
        console.log("Repair result: (" + repairResult + ") The creep does not carry any energy.");
      } else if (repairResult === -7) {
        console.log("Repair result: (" + repairResult + ") The target is not a valid creep object.");
      } else if (repairResult === -9) {
        //console.log("Repair result: (" + repairResult + ")  The target is too far away.");
      } else if (repairResult === -12) {
        console.log("Repair result: (" + repairResult + ") There are no WORK body parts in this creep’s body.");
        //creep.memory.mining = false;
      } else {
        console.log("Nothing to repair");
      }
    },
    
    checkMarketTransaction: function(marketResult) {
        if (marketResult === 0) {
        console.log("Market result: (" + marketResult + ") The operation has been scheduled successfully.");
      } else if (marketResult === -1) {
        console.log("Market result: (" + marketResult + ") 	You don't have a terminal in the target room.");
      } else if (marketResult === -6) {
        console.log("Market result: (" + marketResult + ") 	You don't have enough credits or resource units.");
      } else if (marketResult === -8) {
        console.log("Market result: (" + marketResult + ") 	You cannot execute more than 10 deals during one tick.");
      } else if (marketResult === -10) {
        console.log("Market result: (" + marketResult + ") 	The arguments provided are invalid.");
      }
    },
    
    // --------------------- PATHING AND DISTANCE
    
    getRoomDistance: function(startRoom, endRoom) {
        
        var startEW = startRoom.substring(0,1),
            startEWVal = "",
            startNS,
            startNSVal = "";
        for (var i in startRoom) {
            if (i == 0) {
                if (startRoom[i] == "W" || startRoom[i] == "E") {
                    startEW = startRoom[i];
                }
            } else if (startNS == null && (startRoom[i] == "N" || startRoom[i] == "S")) {
                startNS = startRoom[i];
            } else if (startNS == null && startEW != null) {
                startEWVal += startRoom[i];
            } else if (startEW != null && startNS != null) {
                startNSVal += startRoom[i];
            }
        }
        
        var endEW = endRoom.substring(0,1),
            endEWVal = "",
            endNS,
            endNSVal = "";
        for (var i in endRoom) {
            if (i == 0) {
                if (endRoom[i] == "W" || endRoom[i] == "E") {
                    endEW = endRoom[i];
                }
            } else if (endNS == null && (endRoom[i] == "N" || endRoom[i] == "S")) {
                endNS = endRoom[i];
            } else if (endNS == null && endEW != null) {
                endEWVal += endRoom[i];
            } else if (startEW != null && endEW != null) {
                endNSVal += endRoom[i];
            }
        }
        
        startNSVal = parseInt(startNSVal);
        startEWVal = parseInt(startEWVal);
        endNSVal = parseInt(endNSVal);
        endEWVal = parseInt(endEWVal);
        
        var ewDiff = 0, nsDiff = 0;
        if (startEW == endEW) {
            ewDiff = Math.abs(startEWVal - endEWVal);
        } else {
            ewDiff = startEWVal + endEWVal;
        }
        
        if (startNS == endNS) {
            nsDiff = Math.abs(startNSVal - endNSVal);
        } else {
            nsDiff = startNSVal + endNSVal;
        }
        
        return ewDiff + nsDiff;
    }
    

};