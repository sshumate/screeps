/* controller.repairer.js
 * This will act as a controller to assign manage repairers and the things they should be repairing.
 * 
 * 
 * Here are ideas for how it must function:
 */

var rooms = require('rooms');

var roomsToCheck = ['W12S63', 'W12S64', 'W12S65', 'W11S64', 'W11S65'];

var expeditions = {
    
};

const BUILDING_PCT = .66;   // constant for percentage at which to repair a thing

var repControllerFunctions = {
    
  updateRepairInfo: function() {

    // function for sorting based on HP percent, low to high
    function repairSort(a, b) {
      if ( a[1] === b[1] ) {
        return 0;
      } else {
        return (a[1] < b[1]) ? -1 : 1;
      }
    }

    // make sure our memory objects exist
    if (!Memory.controllers.repair) {
      Memory.controllers.repair = new Object();
    }

    if (!Memory.controllers.repair.repairStructures) {
      Memory.controllers.repair.repairStructures = [];
    }

    if (!Memory.controllers.repair.repairWalls) {
      Memory.controllers.repair.repairWalls = [];
    }

    if (!Memory.controllers.repair.repairRamparts) {
      Memory.controllers.repair.repairRamparts = [];
    }

    // try to get our memory objects into variables
    try {
      var currentRepairStructuresList = Memory.controllers.repair.repairStructures;
      var currentRepairWallsList = Memory.controllers.repair.repairWalls;
      var currentRepairRampartsList = Memory.controllers.repair.repairRamparts;
    } catch (err) {
      console.log(err.message);
      var currentRepairStructuresList = [],
          currentRepairWallsList = [],
          currentRepairRampartsList = [];
    }

    // create an array of our structure IDs
    var structIdArray = new Array();
    for (let i = 0; i < currentRepairStructuresList.length; i++) {
      structIdArray.push(currentRepairStructuresList[i][0]);
    }
    var wallIdArray = new Array();
    for (let i = 0; i < currentRepairWallsList.length; i++) {
      wallIdArray.push(currentRepairWallsList[i][0]);
    }
    var rampIdArray = new Array();
    for (let i = 0; i < currentRepairRampartsList.length; i++) {
      rampIdArray.push(currentRepairRampartsList[i][0]);
    }

    for (var i in roomsToCheck) {

      var checkRoom = roomsToCheck[i];
      console.log('check room: ' + checkRoom);

      // -------------- STRUCTURES (that aren't walls or ramparts)

      // check for structures in need of repair that are not ramparts or walls
      var needRepair = Game.rooms[checkRoom].find(FIND_STRUCTURES, {
          filter: function(object){
              return (object.structureType != STRUCTURE_WALL &&
                      object.structureType != STRUCTURE_RAMPART) &&
                      (object.hits < (BUILDING_PCT * object.hitsMax) &&
                      structIdArray.indexOf(object.id) < 0);
          }
      });

      //console.log('struct repair length: ' + needRepair.length);
      // if there are new things in need of repair, push them to our repair list
      if (needRepair.length > 0) {

        for (var i in needRepair) {
          let percentHP = needRepair[i].hits / needRepair[i].hitsMax;
          currentRepairStructuresList.push([needRepair[i].id, percentHP, needRepair[i].room.name]);
        }

      }

      // -------------- WALLS

      // search for walls
      var wallRepair = Game.rooms[checkRoom].find(FIND_STRUCTURES, {
          filter: function(object){
              return (object.structureType === STRUCTURE_WALL) &&
                     (object.hits < (rooms.getRoom(checkRoom).WALL_HP) && // ??
                     wallIdArray.indexOf(object.id) < 0);
          }
      });

      //console.log('wall repair length: ' + wallRepair.length);
      if (wallRepair.length > 0) {

        for (var i in wallRepair) {
          if (currentRepairWallsList.indexOf(wallRepair[i].id) < 0) {
            //let percentHP = wallRepair[i].hits / wallRepair[i].hitsMax;
            let percentHP = wallRepair[i].hits / rooms.getRoom(checkRoom).WALL_HP;
            currentRepairWallsList.push([wallRepair[i].id, percentHP, wallRepair[i].room.name]);
          }
          console.log(JSON.stringify(currentRepairWallsList));
        }

      }

      // -------------- RAMPARTS

      // search for ramparts
      var rampartRepair = Game.rooms[checkRoom].find(FIND_STRUCTURES, {
          filter: function(object){
              return (object.structureType === STRUCTURE_RAMPART) &&
                     (object.hits < (rooms.getRoom(checkRoom).WALL_HP) && // ??
                     rampIdArray.indexOf(object.id) < 0);
          }
      });

      //console.log('rampart repair length: ' + rampartRepair.length);
      if (rampartRepair.length > 0) {

        for (var i in rampartRepair) {
          if (currentRepairRampartsList.indexOf(rampartRepair[i].id) < 0) {
            //let percentHP = rampartRepair[i].hits / rampartRepair[i].hitsMax;rooms.getRoom(checkRoom).WALL_HP
            let percentHP = rampartRepair[i].hits / rooms.getRoom(checkRoom).WALL_HP;
            currentRepairRampartsList.push([rampartRepair[i].id, percentHP, rampartRepair[i].room.name]);
          }
          console.log(JSON.stringify(currentRepairRampartsList));
        }

      }
    } // end for

    // sort the lists
    currentRepairStructuresList.sort(repairSort);
    currentRepairWallsList.sort(repairSort);
    currentRepairRampartsList.sort(repairSort);

    // put the updated lists into memory
    Memory.controllers.repair.repairStructures = currentRepairStructuresList;
    Memory.controllers.repair.repairWalls = currentRepairWallsList;
    Memory.controllers.repair.repairRamparts = currentRepairRampartsList;
    
  },

  getRepair:function (roomName) {
    //this.updateRepairInfo();
    if (Memory.controllers.repair.repairStructures) {
      var currentRepairStructuresList = Memory.controllers.repair.repairStructures;
      var currentRepairWallsList = Memory.controllers.repair.repairWalls;
      var currentRepairRampartsList = Memory.controllers.repair.repairRamparts;
    } else {
      this.updateRepairInfo();
    }

    if (currentRepairStructuresList.length < 1) {
      this.updateRepairInfo();
    }

    if (currentRepairStructuresList[0][1] < .33) {

      // if our lowest HP item is less than 25%, go to that
      console.log('returning structure because less than 33%');
      var toBeRepaired = currentRepairStructuresList.shift();

    //} else if ( currentRepairRampartsList.length > 0 && Game.getObjectById(currentRepairRampartsList[0][0]).hitsMax < ( .33 * rooms.getRoom(currentRepairRampartsList[0][2]).WALL_HP) ) {
    } else if ( currentRepairRampartsList.length > 0 && currentRepairRampartsList[0][1] < .33 ) {

      // if a rampart is less than 33% of its room-assigned HP, return it
      console.log('returning rampart because less than 33%');
      var toBeRepaired = currentRepairRampartsList.shift();

    } else {

      // otherwise look for something in this room to repair
      console.log('returning structure because else');
      var toBeRepaired = 0;

      for ( var j; j < currentRepairStructuresList.length; j++ ) {

        console.log('currentRepairStructuresList[j][2]: ' + currentRepairList[j][2]);
        if ( currentRepairStructuresList[j][2] === roomName ) {
          var removing = currentRepairStructuresList.splice(j, 1);
          console.log('removing: ' + removing);
          toBeRepaired = currentRepairStructuresList[j];
          break;
        }

      }

      if (toBeRepaired == 0) {
        toBeRepaired = currentRepairStructuresList.shift();
      }
    }

    // put our repair list back in memory and return the ID
    Memory.controllers.repair.repairStructures = currentRepairStructuresList;
    Memory.controllers.repair.repairWalls = currentRepairWallsList;
    Memory.controllers.repair.repairRamparts = currentRepairRampartsList;
    return toBeRepaired[0];

  }
    
};

module.exports = repControllerFunctions;