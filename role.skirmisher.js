var myFunctions = require('functions');

var roleSkirmisher = {
    
    run: function(creep) {
        
        // --------------- BEGIN FUNCTIONS
        
        function suppressKeepers() {
            var target = creep.pos.findClosestByPath(FIND_HOSTILE_CREEPS);
            if (target) {
                creep.moveTo(target);
                healSelf();
                if (creep.moveTo(target) == ERR_NO_BODYPART) {
                    creep.attack(target);
                }
                /*
                if (creep.attack(target) == ERR_NOT_IN_RANGE ) {
                    healSelf();
                }
                */
            } else {
                healSelf();
                if ( !healOthers() ) {
                    var lairs = creep.room.find(FIND_STRUCTURES, {
                        filter: { structureType: STRUCTURE_KEEPER_LAIR }
                    });
                    lairs.sort ( function(l1, l2) { return l1.ticksToSpawn - l2.ticksToSpawn; } );
                    creep.moveTo(lairs[0],  {  visualizePathStyle: {
                                                fill: 'transparent',
                                                stroke: '#fff',
                                                lineStyle: 'dashed',
                                                strokeWidth: .15,
                                                opacity: .25}});
                }
            }
        }
        
        function healSelf() {
            if (creep.hits < creep.hitsMax) {
                creep.heal(creep);
            }
        }
        
        function healOthers() {
            // maybe heal others later?
            var friendlies = creep.room.find(FIND_MY_CREEPS, {
                filter: (wimp) => {
                    return creep.pos.getRangeTo(wimp) <= 5 && wimp.hits < wimp.hitsMax && wimp.id != creep.id;
                }
            });
            
            if (friendlies.length > 0) {
                creep.heal(friendlies[0]);
                creep.moveTo(friendlies[0],  {  visualizePathStyle: {
                                                fill: 'transparent',
                                                stroke: '#fff',
                                                lineStyle: 'dashed',
                                                strokeWidth: .15,
                                                opacity: .25}});
                return true;
            } else {
                return false;
            }
        }
        
        function checkRoom() {
            if (creep.room.name == creep.memory.destination) {
                return true;
            } else {
                var destFlag = 'room' + creep.memory.destination;
                return false;
            }
            
        }
        
        function setDestination() {
            var skDest;
            var roomName = creep.memory.room;
            if ( roomName == 'W16S67') {
                skDest = 'W16S66';
            } else if ( roomName == 'W13S65' ) {
                skDest = 'W14S65';
            } else if ( roomName == 'W15S73' ) {
                skDest = 'W15S74';
            }
            creep.memory.destination = skDest;
        }
        
        // --------------- END FUNCTIONS
        // --------------- BEGIN PROGRAM LOGIC
        
        if (creep.memory.destination == undefined) {
            setDestination();
        }
        
        // find out how many ticks this creep has left to live, and if he is fading away, set memory
        myFunctions.monitorLife(creep, 50);
        
        if ( checkRoom() ) {
            suppressKeepers();
        } else {
            var destFlag = 'room' + creep.memory.destination;
            creep.moveTo(Game.flags[destFlag],  {  visualizePathStyle: {
                                                fill: 'transparent',
                                                stroke: '#fff',
                                                lineStyle: 'dashed',
                                                strokeWidth: .15,
                                                opacity: .25}});
        }
        
        // --------------- END PROGRAM LOGIC
        
    }
    
};

module.exports = roleSkirmisher;